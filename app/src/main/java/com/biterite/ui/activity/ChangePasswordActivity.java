/**
 * Created by Amit Kumar Mandal on 10/19/2016.
 */
package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;

public class ChangePasswordActivity extends BaseActivity {
    EditText edtOldPass, edtNewPass, edtNewCnfPass;
    Button btnSave;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        edtOldPass = (EditText) findViewById(R.id.edtOldPass);
        edtNewPass = (EditText) findViewById(R.id.edtNewPass);
        edtNewCnfPass = (EditText) findViewById(R.id.edtNewCnfPass);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_CHANGE_PASSWORD);
                }
            }
        });
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtOldPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtNewPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtNewCnfPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (!edtNewPass.getText().toString().equals(edtNewCnfPass.getText().toString())) {
            ToastUtils.showToast(this, "Confirm password do not match");
            return false;
        }
        return true;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        ((BaseActivity) this).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHANGE_PASSWORD:
                String url = ApiConstants.URL_CHANGE_PASSWORD;
                className = String.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getHashParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getHashParams(int reqType) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (reqType == ApiConstants.REQUEST_CHANGE_PASSWORD) {
            hashMap.put(ApiConstants.PARAMS.USER_ID, BiteRitePreference.getInstance().getUserId());
            hashMap.put(ApiConstants.PARAMS.OLDPASS, edtOldPass.getText().toString());
            hashMap.put(ApiConstants.PARAMS.NEWPASS, edtNewPass.getText().toString());
        }
        return hashMap;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            ((BaseActivity) this).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_CHANGE_PASSWORD:
                    String changePasswordResponse = (String) responseObject;
                    if (changePasswordResponse.equals("1")) {
                        BiteRitePreference.logoutUser();
                        ToastUtils.showToast(this, "Password updated successfully, Please login again!");
                        Intent i = new Intent(this, HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
