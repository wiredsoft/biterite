package com.biterite.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.AddToCartModel;
import com.krapps.ui.BaseActivity;

/**
 * Created by K.Agg on 18-10-2016.
 */
public class MenuDetailsActivity extends BaseActivity {
    Button btnAddtoCart;
    TextView txtprice, txtQuantity, txtItemName;
    AddToCartModel addToCartModel;
    float price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);

        txtprice = (TextView) findViewById(R.id.txtprice);
        // txtplus = (ImageView) findViewById(R.id.txtplus);
        txtQuantity = (TextView) findViewById(R.id.txtQuantity);
        // txtMinus = (ImageView) findViewById(R.id.txtMinus);
        btnAddtoCart = (Button) findViewById(R.id.btnAddtoCart);
        txtItemName = (TextView) findViewById(R.id.txtItemName);

        addToCartModel = new AddToCartModel();

        txtItemName.setText(getIntent().getStringExtra("productName"));
        txtprice.setText("\u20B9" + getIntent().getStringExtra("productPrice"));
        price = Float.parseFloat(getIntent().getStringExtra("productPrice"));

        // txtplus.setOnClickListener(this);
        // txtMinus.setOnClickListener(this);
        btnAddtoCart.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.txtplus:
                if (Integer.parseInt(txtQuantity.getText().toString()) < 10) {
                    updateAmount(1);
                }
                break;
            case R.id.txtMinus:
                if (Integer.parseInt(txtQuantity.getText().toString()) > 1) {
                    updateAmount((-1));
                }
                break;*/
            case R.id.btnAddtoCart:
                addToCartModel.setProductAmount(Float.parseFloat(getIntent().getStringExtra("productPrice")));
                addToCartModel.setProductName(getIntent().getStringExtra("productName"));
                addToCartModel.setProductQuantity("1");
                addToCartModel.setProductTypeName(getIntent().getStringExtra("productType"));
                addToCartModel.setProductId(getIntent().getStringExtra("productCode"));
                addToCartModel.setRestaurentId(getIntent().getStringExtra("resturent_id"));
                if (addItemToCart(addToCartModel)) {
                    finish();
                }


                break;

            default:
                super.onClick(view);
        }
    }

    public void updateAmount(int change) {
        String quantity = Integer.toString((Integer.parseInt(txtQuantity.getText().toString()) + change));
        txtQuantity.setText(quantity);
        Float amount = Float.parseFloat(quantity) * price;
        txtprice.setText(amount + "");

    }


}
