package com.biterite.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.PreviousOrderResponse;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Saurabh Vardani on 14-11-2016.
 */

public class PreviousOrdersActivity extends BaseActivity {

    List<PreviousOrderResponse.ObjUserOrder> objUserOrders = new ArrayList<>();
    String pos;
    TextView txtRoundPlaced, txtRoundCooked, txtRoundDelivered, txtPlaced, txtCooked, txtDelivered, txtAddress, txtCancelled;
    LinearLayout llStatus;
    View view3, view4;
    ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_orders_details);

        txtRoundPlaced = (TextView) findViewById(R.id.txtRoundPlaced);
        txtRoundCooked = (TextView) findViewById(R.id.txtRoundCooked);
        txtRoundDelivered = (TextView) findViewById(R.id.txtRoundDelivered);
        txtPlaced = (TextView) findViewById(R.id.txtPlaced);
        txtCooked = (TextView) findViewById(R.id.txtCooked);
        txtDelivered = (TextView) findViewById(R.id.txtDelivered);
        txtAddress = (TextView) findViewById(R.id.txtAddress);
        txtCancelled = (TextView) findViewById(R.id.txtCancelled);
        llStatus = (LinearLayout) findViewById(R.id.llStatus);
        view3 = (View) findViewById(R.id.view3);
        view4 = (View) findViewById(R.id.view4);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            pos = bundle.getString("position");

        hitApiRequest(ApiConstants.REQUEST_PREVIOUS_ORDER_LIST);
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_PREVIOUS_ORDER_LIST:
                showProgressDialog();
                String url = ApiConstants.URL_PREVIOUS_ORDERS_LIST + BiteRitePreference.getInstance().getPhoneNo();
                className = PreviousOrderResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                hitApiRequest(reqType);
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        if (this == null) {
            return;
        }
        ((BaseActivity) this).removeProgressDialog();

        try {
            if (!isSuccess) {
                Toast.makeText(this, "Some error occured", Toast.LENGTH_SHORT).show();
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_PREVIOUS_ORDER_LIST:

                    PreviousOrderResponse previousOrderResponse = (PreviousOrderResponse) responseObject;
                    if (previousOrderResponse.getStatusCode() == 1101) {
                        if (previousOrderResponse.getObjUserOrder().size() > 1) {
                            objUserOrders.add(previousOrderResponse.getObjUserOrder().get(Integer.parseInt(pos)));
                            setUI();
                        } else {
                            Toast.makeText(this, "Something went wrong !", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setUI() {

        txtAddress.setText(objUserOrders.get(0).getAddress());
        if (objUserOrders.get(0).getOrderStatus().equals("Placed")) {
            txtRoundPlaced.setVisibility(View.VISIBLE);
            ViewGroup.LayoutParams params = txtRoundPlaced.getLayoutParams();
            params.height = 100;
            params.width = 100;
            txtRoundPlaced.setLayoutParams(params);
            txtRoundCooked.setBackgroundResource(R.drawable.rounded_corner_white);
            txtRoundDelivered.setBackgroundResource(R.drawable.rounded_corner_white);
            txtPlaced.setVisibility(View.VISIBLE);
        } else if (objUserOrders.get(0).getOrderStatus().equals("Cancelled")) {
            llStatus.setVisibility(View.GONE);
            txtCancelled.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        } else if (objUserOrders.get(0).getOrderStatus().equals("Ready to dispatch")) {
            ViewGroup.LayoutParams params = txtRoundCooked.getLayoutParams();
            params.height = 100;
            params.width = 100;
            txtRoundCooked.setLayoutParams(params);
            txtRoundPlaced.setVisibility(View.VISIBLE);
            txtRoundCooked.setVisibility(View.VISIBLE);
            txtRoundDelivered.setBackgroundResource(R.drawable.rounded_corner_white);
            txtCooked.setVisibility(View.VISIBLE);
        } else if (objUserOrders.get(0).getOrderStatus().equals("Delivered")) {
            ViewGroup.LayoutParams params = txtRoundDelivered.getLayoutParams();
            params.height = 100;
            params.width = 100;
            txtRoundDelivered.setLayoutParams(params);
            txtRoundPlaced.setVisibility(View.VISIBLE);
            txtRoundCooked.setVisibility(View.VISIBLE);
            txtRoundDelivered.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            txtDelivered.setVisibility(View.VISIBLE);
        }
        removeProgressDialog();
    }
}