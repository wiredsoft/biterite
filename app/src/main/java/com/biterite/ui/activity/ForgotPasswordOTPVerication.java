package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.constants.AppConstants;
import com.biterite.model.ChangePasswordResponse;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;

/**
 * Created by Amit Kumar Mandal on 11/5/2016.
 */
public class ForgotPasswordOTPVerication extends BaseActivity {
    EditText edt_otp, edt_pass, edt_cnfpass;
    TextView txt_save;
    String phoneNumber, url;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_otp_verification);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        phoneNumber = getIntent().getExtras().getString(AppConstants.EXTRA_PHONE_NO);
        edt_otp = (EditText) findViewById(R.id.edt_otp);
        edt_pass = (EditText) findViewById(R.id.edt_pass);
        edt_cnfpass = (EditText) findViewById(R.id.edt_cnfpass);
        txt_save = (TextView) findViewById(R.id.txt_save);
        txt_save.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_save:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_OTP_VERFICATION);
                }
                break;

            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_OTP_VERFICATION:
                url = ApiConstants.URL_FORGOT_PASSWORD_POST;
                className = ChangePasswordResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_OTP_VERFICATION) {
            params.put(ApiConstants.PARAMS.OTP_FORGOT, edt_otp.getText().toString());
            params.put(ApiConstants.PARAMS.MOBILE_NUMBER, phoneNumber);
            params.put(ApiConstants.PARAMS.USER_PASSWORD, edt_pass.getText().toString());
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_OTP_VERFICATION:
                    ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) responseObject;
                    if (changePasswordResponse.getStatusCode() == 10001) {
                        Bundle bundle = getIntent().getExtras();
                        phoneNumber = bundle.getString("mobile");

                        AlertDialogUtils.showAlertDialog(this, "BiteRite", "Wow! Password updated successfully", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(ForgotPasswordOTPVerication.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        });

                    } else {
                        ToastUtils.showToast(this, "Wrong OTP");
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edt_otp.getText().toString())) {
            ToastUtils.showToast(this, "Please enter otp.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edt_pass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edt_cnfpass.getText().toString())) {
            ToastUtils.showToast(this, "Please confirm Password");
            return false;
        } else if (!edt_pass.getText().toString().equals(edt_cnfpass.getText().toString())) {
            ToastUtils.showToast(this, "The password and confirm password do not match");
            return false;
        }
        return true;
    }
}
