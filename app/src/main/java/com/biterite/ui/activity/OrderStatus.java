package com.biterite.ui.activity;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.biterite.R;
import com.krapps.ui.BaseActivity;

/**
 * Created by mohitbaweja on 06/12/16.
 */

public class OrderStatus extends BaseActivity {

    private ProgressBar progressBar;
    TextView txtProgress,txtRoundPlaced;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);


        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        txtProgress  = (TextView) findViewById(R.id.tv_progress_circle);

        progressBar.setProgress(0);   // Main Progress
        progressBar.setSecondaryProgress(100); // Secondary Progress
        progressBar.setMax(100); // Maximum Progress
        progressBar.setProgress(125);
        txtRoundPlaced = (TextView) findViewById(R.id.txtRoundPlaced);

        //  txtRoundPlaced.setBackgroundDrawable(getResources().getDrawable(R.drawable.order_status_one_active));


    }
}
