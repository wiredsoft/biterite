package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.ui.fragment.RestaurentMenuFragment;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;

/**
 * Created by Saurabh Vardani on 19-10-2016.
 */

public class RestaurentListActivity extends BaseActivity {

    FloatingActionButton fab;
    TextView txtQuantity;
    private Animation scale_up, scale_down;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.fragment_restaurent_details);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        scale_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_up);
        scale_down = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale_down);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        txtQuantity = (TextView) findViewById(R.id.txtQuantity);
        fab.hide();
        txtQuantity.setVisibility(View.GONE);
        if (getCartItemSize() != 0) {
            fab.show();
            fab.startAnimation(scale_up);
            fab.setOnClickListener(this);
            txtQuantity.setText(getCartItemSize() + "");
            txtQuantity.setVisibility(View.VISIBLE);
        }
        if (arg0 == null) {
            Bundle bundle = new Bundle();
            bundle.putString("resturent_id", getIntent().getStringExtra("resturent_id"));
            replaceFragmentTwo(RestaurentMenuFragment.class.getSimpleName(), new RestaurentMenuFragment(), bundle, false);
        }
    }

    @Override
    protected void onRestart() {
        if (getCartItemSize() != 0) {
            fab.show();
            fab.startAnimation(scale_up);
            fab.setOnClickListener(this);
            txtQuantity.setText(getCartItemSize() + "");
            txtQuantity.setVisibility(View.VISIBLE);
        } else {
            if (fab.getVisibility() == View.VISIBLE) {
                fab.hide();
                fab.setClickable(false);
                fab.startAnimation(scale_up);
                txtQuantity.setVisibility(View.GONE);
            }
        }
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.FrameLayout);
        String one = currentFragment.getClass().toString();
        String two = "class com.biterite.ui.fragment.RestaurentMenuFragment";
        if (getCartItemSize() > 0 && one.equals(two)) {
            AlertDialogUtils.showAlertDialogWithTwoButtons(this, "BiteRite", "Are you sure you'd like to change restaurants? Your current order will be lost.", "Ok", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    if (buttonId != 0) {
                        removeAllItemToCart();
                        onBackPressed();
                    }
                }
            });
        } else {
            super.onBackPressed();
        }


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                Intent intent = new Intent(this, AddToCartActivity.class);
                startActivity(intent);
                break;
        }
    }


}
