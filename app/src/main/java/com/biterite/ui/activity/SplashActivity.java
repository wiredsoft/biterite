package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.biterite.R;
import com.krapps.ui.BaseActivity;


/**
 * Created by Saurabh Vardani on 04-09-2016.
 */
public class SplashActivity extends BaseActivity {
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /*if(BiteRitePreference.getInstance().getLoggedIn()){
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }else {*/
                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                //     }
            }
        }, SPLASH_TIME_OUT);
    }
}

