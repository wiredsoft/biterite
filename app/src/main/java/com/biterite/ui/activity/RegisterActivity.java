/**
 * Created by Amit Kumar Mandal on 10/17/2016.
 */

package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.constants.AppConstants;
import com.biterite.model.RegisterResponse;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;


public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private EditText edtName, edtNumber, edtEmail, edtPassword, edtConfirmPassword;
    private TextView txtRegisterbtn;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        edtName = (EditText) findViewById(R.id.edtName);
        edtNumber = (EditText) findViewById(R.id.edtNumber);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);
        txtRegisterbtn = (TextView) findViewById(R.id.txtRegisterbtn);
        txtRegisterbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtRegisterbtn:
                if (validateData()) {
                    if (((CheckBox) findViewById(R.id.checkboxAgree)).isChecked()) {
                        hitApiRequest(ApiConstants.REQUEST_REGISTER);
                    } else {
                        ToastUtils.showToast(this, "Please check terms and condition");
                    }
                }
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtName.getText().toString())) {
            ToastUtils.showToast(this, "Please enter name.");
            return false;
        } else if (!StringUtils.isValidMobileNumber(edtNumber.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile number");
            return false;
        } else if (!StringUtils.isValidEmail(edtEmail.getText().toString(), false)) {
            ToastUtils.showToast(this, "Please enter valid Email.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtConfirmPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (!edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            ToastUtils.showToast(this, "Confirm password do not match");
            return false;
        }
        return true;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_REGISTER:
                url = ApiConstants.URL_REGISTER;
                className = RegisterResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_REGISTER) {
            params.put(ApiConstants.PARAMS.USERNAME, edtName.getText().toString().trim());
            params.put(ApiConstants.PARAMS.MOBILE, edtNumber.getText().toString().trim());
            params.put(ApiConstants.PARAMS.EMAILID, edtEmail.getText().toString().trim());
            params.put(ApiConstants.PARAMS.PASSWORD1, edtConfirmPassword.getText().toString().trim());
            /*params.put(ApiConstants.PARAMS.PASSWORD1, StringUtils.getBase64String(edtConfirmPassword.getText().toString()));*/
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_REGISTER:
                    RegisterResponse registerResponse = (RegisterResponse) responseObject;

                    if (registerResponse.getStatusCode() == 4001) {
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent(RegisterActivity.this, OtpActivity.class);
                        bundle.putString("name", edtName.getText().toString());
                        bundle.putString("email", edtEmail.getText().toString());
                        bundle.putString("mobile", edtNumber.getText().toString());
                        bundle.putString("password", edtConfirmPassword.getText().toString());
                        BiteRitePreference.getInstance().setUserId(registerResponse.getUserid() + "");
                        intent.putExtra(AppConstants.EXTRA_PHONE_NO, edtNumber.getText().toString());
                        intent.putExtra(AppConstants.EXTRA_USERNAME, edtName.getText().toString());
                        intent.putExtras(bundle);
                        ToastUtils.showToast(this, registerResponse.getStatusMessage());
                        startActivity(intent);
                    } else if (registerResponse.getStatusCode() == 4003) {
                        ToastUtils.showToast(this, registerResponse.getStatusMessage());
                    } else {
                        ToastUtils.showToast(this, registerResponse.getStatusMessage());
                    }
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ToastUtils.showToast(this, ex.getMessage());

        }
    }


}
