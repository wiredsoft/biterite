package com.biterite.ui.activity;

import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.ui.fragment.DealsFragment;
import com.biterite.ui.fragment.LoginChooseFragment;
import com.biterite.ui.fragment.MyAccountFragment;
import com.biterite.ui.fragment.OrdersFragment;
import com.biterite.ui.fragment.RestaurantFragment;
import com.biterite.utils.BiteRitePreference;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ToastUtils;

/**
 * Created by kunal on 10/17/2016.
 */
public class HomeActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout llNavigation;
    public PackageInfo pInfo;
    TextView[] textView;
    private int[] imgInactive;
    private int[] imgActive;


    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_home);

      /*  pInfo = null;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            if(verCode==1.0){
                AlertDialogUtils.showAlertDialog(this, "BiteRite", "Please update your app!", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        Intent intent = new Intent(Intent.ACTION_VIEW , Uri.parse("market://details?id=com.app.biterite"));
                        startActivity(intent);
                    }
                });

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_img);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        getSupportActionBar().setHomeButtonEnabled(true);
        setUpDrawer();
        textView = new TextView[5];

        imgInactive = new int[]{
                R.drawable.shop_menu,
                R.drawable.deal_menu,
                R.drawable.orders_menu,
                R.drawable.account_menu,
                R.drawable.settings_menu,
        };

        imgActive = new int[]{
                R.drawable.shop_menu_orange,
                R.drawable.deal_menu_orange,
                R.drawable.orders_menu_orange,
                R.drawable.account_menu_orange,
                R.drawable.settings_menu_orange,
        };


        llNavigation = (LinearLayout) findViewById(R.id.llNavigation);


        textView[0] = (TextView) findViewById(R.id.txtRestaurants);
        textView[1] = (TextView) findViewById(R.id.txtDeals);
        textView[2] = (TextView) findViewById(R.id.txtOrders);
        textView[3] = (TextView) findViewById(R.id.txtAccounts);
        textView[4] = (TextView) findViewById(R.id.txtSettings);

        textView[0].setOnClickListener(this);
        textView[1].setOnClickListener(this);
        textView[2].setOnClickListener(this);
        textView[3].setOnClickListener(this);
        textView[4].setOnClickListener(this);
        textView[0].performClick();
    }

    private void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                // super.onDrawerClosed(view);
                getActionBar().setTitle("BiteRite");
                mDrawerLayout.openDrawer(GravityCompat.START);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                // super.onDrawerOpened(drawerView);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                getActionBar().setTitle("BiteRite");
                invalidateOptionsMenu();
            }
        };
    }

    @Override
    public void onClick(View view) {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
        switch (view.getId()) {
            case R.id.txtRestaurants:
                replaceFragment(null, new RestaurantFragment(), null, false);
                ChangeColor(0);
                break;

            case R.id.txtDeals:
                replaceFragment(null, new DealsFragment(), null, true);
                ChangeColor(1);
                break;
            case R.id.txtOrders:
                ChangeColor(2);
                replaceFragment(null, new OrdersFragment(), null, true);
                break;
            case R.id.txtAccounts:
                ChangeColor(3);
                if (BiteRitePreference.getInstance().getLoggedIn()) {
                    replaceFragment(null, new MyAccountFragment(), null, false);

                } else {
                    replaceFragment(null, new LoginChooseFragment(), null, false);
                }
                break;
            case R.id.txtSettings:
                ChangeColor(4);
                ToastUtils.showToast(this, "We are currently working on it");
                replaceFragment(null, new RestaurantFragment(), null, false);
                ChangeColor(0);
                break;
            default:
                super.onClick(view);
        }
    }

    public void ChangeColor(int s) {

        for (int a = 0; a < textView.length; a++) {

            textView[a].setTextColor(getResources().getColor(R.color.nav_text_color));
            textView[a].setCompoundDrawablesWithIntrinsicBounds(imgInactive[a], 0, 0, 0);
            if (a == s) {
                textView[a].setTextColor(getResources().getColor(R.color.nav_selectd_text_color));
                textView[a].setCompoundDrawablesWithIntrinsicBounds(imgActive[a], 0, 0, 0);
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    public void onBackPressed() {
        if (BiteRitePreference.getInstance().getShowDilogorNot()) {
            this.showExitDialog();
        } else {
            replaceFragment(null, new RestaurantFragment(), null, false);
            ChangeColor(0);
        }
    }

    //show AlertDialog when device back button is Pressed
    private void showExitDialog() {
        AlertDialogUtils.showAlertDialogWithTwoButtons(this, "BiteRite", "Are you sure you want to exit ?", "Ok", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
            @Override
            public void onButtonClick(int buttonId) {
                if (buttonId != 0) {
                    moveTaskToBack(false);
                }
            }
        });
    }

}
