package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.constants.AppConstants;
import com.biterite.model.ChangePasswordResponse;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

/**
 * Created by Amit Kumar Mandal on 11/4/2016.
 */
public class EnterMobileActivity extends BaseActivity {
    EditText edt_mobile;
    TextView btnSubmit;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_mobile);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        edt_mobile = (EditText) findViewById(R.id.edt_mobile);
        btnSubmit = (TextView) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:

                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_FORGOT_PASSWORD);
                }
                break;
            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_FORGOT_PASSWORD:
                ((BaseActivity) this).showProgressDialog();
                String url = ApiConstants.URL_FORGOT_PASSWORD_GET + edt_mobile.getText().toString();
                className = ChangePasswordResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                hitApiRequest(reqType);
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        if (this == null) {
            return;
        }
        ((BaseActivity) this).removeProgressDialog();

        try {
            if (!isSuccess) {
                Toast.makeText(this, "Some error occured", Toast.LENGTH_SHORT).show();
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:

                    ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) responseObject;
                    if (changePasswordResponse.getStatusCode() == 9001) {
                        ToastUtils.showToast(this, changePasswordResponse.getStatusMessage());
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent(EnterMobileActivity.this, ForgotPasswordOTPVerication.class);
                        bundle.putString("mobile", edt_mobile.getText().toString());
                        intent.putExtra(AppConstants.EXTRA_PHONE_NO, edt_mobile.getText().toString());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {
                        Toast.makeText(this, "Wrong Mobile number", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edt_mobile.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Mobile Number");
            return false;
        }
        return true;
    }
}
