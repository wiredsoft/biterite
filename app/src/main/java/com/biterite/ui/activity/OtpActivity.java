package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.constants.AppConstants;
import com.biterite.model.OTPResponse;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;

/**
 * Created by Amit Kumar Mandal on 10/17/2016.
 */
public class OtpActivity extends BaseActivity {
    EditText edtOTP;
    String url, phoneNumber, userName, password;
    TextView txtSubmit;
    String name, email, number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        phoneNumber = getIntent().getExtras().getString(AppConstants.EXTRA_PHONE_NO);
        userName = getIntent().getExtras().getString(AppConstants.EXTRA_USERNAME);
        password = getIntent().getExtras().getString("password");
        email = getIntent().getExtras().getString("email");
        edtOTP = (EditText) findViewById(R.id.edtOTP);
        txtSubmit = (TextView) findViewById(R.id.txtSubmit);
        txtSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSubmit:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_VERIFY_OTP);
                }
                break;
            default:
                super.onClick(view);
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtOTP.getText().toString())) {
            ToastUtils.showToast(this, "Please enter otp.");
            return false;
        }
        return true;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_VERIFY_OTP:
                url = ApiConstants.URL_VERIFY_OTP;
                className = OTPResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_VERIFY_OTP) {
            params.put(ApiConstants.PARAMS.USER_ID_OTP, BiteRitePreference.getInstance().getUserId());
            params.put(ApiConstants.PARAMS.OTP, edtOTP.getText().toString().trim());
            params.put(ApiConstants.PARAMS.EMAILID, email);
            params.put(ApiConstants.PARAMS.PASSWORD1, password);
            /*params.put(ApiConstants.PARAMS.PASSWORD1, StringUtils.getBase64String(edtConfirmPassword.getText().toString()));*/
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_VERIFY_OTP:
                    OTPResponse OTPResponse = (OTPResponse) responseObject;
                    if (OTPResponse.getStatusCode().equals("5001")) {
                        Bundle bundle = getIntent().getExtras();
                        name = bundle.getString("name");
                        email = bundle.getString("email");
                        number = bundle.getString("mobile");
                        BiteRitePreference.getInstance().setLoggedIn(true);
                        BiteRitePreference.getInstance().setUserName(name);
                        BiteRitePreference.getInstance().setEmail(email);
                        BiteRitePreference.getInstance().setPhoneNo(number);
                        AlertDialogUtils.showAlertDialog(this, "BiteRite", "Your login credentials are in your message inbox :)", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent i = new Intent(OtpActivity.this, HomeActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });


                    } else {
                        ToastUtils.showToast(this, "Wrong OTP");
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
