package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.LoginResponse;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;


/**
 * Created by kunal on 10/17/2016.
 */
public class LoginActivity extends BaseActivity {

    private EditText edtMobile, edtPassword;
    private TextView Login, txtForgotPass;

    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);

        edtMobile = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        Login = (TextView) findViewById(R.id.txtLoginScreen);
        Login.setOnClickListener(this);
        txtForgotPass = (TextView) findViewById(R.id.txtForgotPass);
        txtForgotPass.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtLoginScreen:

                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_LOGIN);
                }
                break;
            case R.id.txtForgotPass:
                Intent i = new Intent(this, EnterMobileActivity.class);
                startActivity(i);
                break;

            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_LOGIN:
                ((BaseActivity) this).showProgressDialog();
                String url = ApiConstants.URL_LOGIN;
                className = LoginResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                hitApiRequest(reqType);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();

        if (reqType == ApiConstants.REQUEST_LOGIN) {
            params.put(ApiConstants.PARAMS.USER_NAME, edtMobile.getText().toString());
            params.put(ApiConstants.PARAMS.PASSWORD, edtPassword.getText().toString());

        }
        return params;


    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        if (this == null) {
            return;
        }
        ((BaseActivity) this).removeProgressDialog();

        try {
            if (!isSuccess) {
                Toast.makeText(getApplicationContext(), "Some error occured", Toast.LENGTH_SHORT).show();
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN:

                    LoginResponse loginResponse = (LoginResponse) responseObject;
                    if (loginResponse.getStatus() == 6001) {
                        BiteRitePreference.getInstance().setLoggedIn(true);
                        BiteRitePreference.getInstance().setUserId(loginResponse.getStatus() + "");
                        BiteRitePreference.getInstance().setUserName(loginResponse.getUserName());
                        BiteRitePreference.getInstance().setEmail(loginResponse.getEmailID());
                        BiteRitePreference.getInstance().setPhoneNo(edtMobile.getText().toString());
                        Intent intent = new Intent(this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "wrong username and password", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtMobile.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Mobile Number");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter password");
            return false;
        }
        return true;
    }


}

