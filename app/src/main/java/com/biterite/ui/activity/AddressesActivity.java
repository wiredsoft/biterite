package com.biterite.ui.activity;

import android.os.Bundle;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.AddressListResponse;
import com.biterite.ui.fragment.AddNewAddressFragment;
import com.biterite.ui.fragment.ShowAddressFragment;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

/**
 * Created by Saurabh Vardani on 07-11-2016.
 */

public class AddressesActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addresses);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);

        hitApiRequest(ApiConstants.REQUEST_LIST_ADDRESS);


    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }

        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_LIST_ADDRESS:
                ((BaseActivity) this).showProgressDialog();
                String url = ApiConstants.URL_LIST_ADDRESS + BiteRitePreference.getInstance().getUserId();
                className = AddressListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;

            }
            switch (reqType) {
                case ApiConstants.REQUEST_LIST_ADDRESS:
                    AddressListResponse addressListResponse = (AddressListResponse) responseObject;
                    if (addressListResponse.getStatusCode() == 8001) {
                        replaceFragmentTwo(AddressesActivity.class.getSimpleName(), new ShowAddressFragment(), null, false);

                    } else {
                        ToastUtils.showToast(this, addressListResponse.getStatusMessage());
                        ((BaseActivity) this).replaceFragmentTwo(null, new AddNewAddressFragment(), null, false);
                    }

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
