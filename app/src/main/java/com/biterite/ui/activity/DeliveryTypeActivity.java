package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.biterite.R;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ToastUtils;

/**
 * Created by K.Agg on 20-10-2016.
 */
public class DeliveryTypeActivity extends BaseActivity {

    RadioGroup rbDeliveryType;
    Button btnProceed;
    int a = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_type);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        btnProceed = (Button) findViewById(R.id.btnProceed);
        rbDeliveryType = (RadioGroup) findViewById(R.id.rbDeliveryType);
        btnProceed.setOnClickListener(this);
        rbDeliveryType.clearCheck();
        rbDeliveryType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rbDelivery) {
                    a = 1;
                } else if (checkedId == R.id.rbPickUp) {
                    a = 2;
                }

            }

        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnProceed:
                if (a == 2) {
                    Intent intent = new Intent(this, AddressesActivity.class);
                    startActivity(intent);
                } else if (a == 1) {
                    Intent intent = new Intent(this, DoPaymentActivity.class);
                    intent.putExtra("address","Pickup");
                    startActivity(intent);
                } else {
                    ToastUtils.showToast(this, "please select one");

                }

                break;
        }

    }
}


