package com.biterite.ui.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.ui.fragment.LoginAfterCartFragment;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.krapps.ui.BaseActivity;

/**
 * Created by kunal on 11/7/2016.
 */
public class LoginAfterCartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_after_cart);

        replaceFragment(LoginAfterCartActivity.class.getSimpleName(), new LoginAfterCartFragment(), null, false);

    }
}
