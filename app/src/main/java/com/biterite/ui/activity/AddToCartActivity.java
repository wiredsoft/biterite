package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.AddToCartModel;
import com.biterite.ui.adapter.AddToCartAdapter;
import com.biterite.utils.BiteRitePreference;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ToastUtils;

import java.util.List;

/**
 * Created by Saurabh Vardani on 19-10-2016.
 */
public class AddToCartActivity extends BaseActivity {
    Button btnAddtoCart;
    ListView listview;
    AddToCartAdapter addToCartAdapter;
    private List<AddToCartModel> addToCartModelList;
    private String[] quantity;
    private TextView txtTotal;
    private int pos;
    private float total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        listview = (ListView) findViewById(R.id.listview);
        txtTotal = (TextView) findViewById(R.id.txtTotalRs);
        quantity = getResources().getStringArray(R.array.quantity);
        btnAddtoCart = (Button) findViewById(R.id.btnAddtoCart);
        btnAddtoCart.setOnClickListener(this);
        addToCartAdapter = new AddToCartAdapter(this, quantity, AddToCartActivity.this, txtTotal);
        listview.setAdapter(addToCartAdapter);
        addToCartModelList = BiteRitePreference.getInstance().getCart();
        txtTotal.setText("\u20B9" + BiteRitePreference.getInstance().totalCostCart());
        setAdapter(addToCartModelList);
    }

    private void setAdapter(List<AddToCartModel> addToCartModelList) {
        addToCartAdapter.setListData(addToCartModelList);
        addToCartAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnAddtoCart:
                if (BiteRitePreference.getInstance().getLoggedIn()) {
                    if (getCartItemSize() > 0) {
                        Intent intent = new Intent(this, DeliveryTypeActivity.class);
                        startActivity(intent);
                    } else {
                        ToastUtils.showToast(this, "Cart is empty");
                    }
                } else {
                    Intent intent = new Intent(this, LoginAfterCartActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.imgCross:
                pos = (int) view.getTag();
                addToCartModelList.remove(pos);
                addToCartAdapter.notifyDataSetChanged();
                BiteRitePreference.getInstance().setCart(addToCartModelList);
                if (addToCartModelList.size() > 0) {
                    txtTotal.setText("\u20B9" + BiteRitePreference.getInstance().totalCostCart());
                } else {
                    txtTotal.setText("\u20B9" + "0");
                }
                break;
        }
    }
}
