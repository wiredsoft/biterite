package com.biterite.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.AddToCartListResponse;
import com.biterite.model.AddToCartModel;
import com.biterite.model.AddToCartResponse;
import com.biterite.model.CodResponse;
import com.biterite.utils.BiteRitePreference;
import com.google.gson.Gson;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.network.VolleyJsonRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saurabh Vardani on 21-10-2016.
 */

public class DoPaymentActivity extends BaseActivity implements UpdateJsonListener.onUpdateViewListener {

    Button btnProceed;
    private List<AddToCartModel> addToCartModelList= new ArrayList<>();
    private AddToCartResponse addToCartResponse;
    private List<AddToCartResponse> addToCartResponseList = new ArrayList<>();
    RadioGroup rgPayment;
    int a = 0;
    String address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_payment);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            address = bundle.getString("address");

        rgPayment = (RadioGroup) findViewById(R.id.rgPayment);
        rgPayment.clearCheck();
        rgPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.rbPaytm) {
                    a = 3;
                }else if (checkedId == R.id.rbPayCash) {
                    a = 5;
                }

            }

        });

        addToCartModelList = new ArrayList<>();
        btnProceed = (Button) findViewById(R.id.btnProceed);
        addToCartModelList = BiteRitePreference.getInstance().getCart();


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.toolbar_icon);
        btnProceed.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnProceed:

                if (a == 5) {
                    setValues();
                } else {
                    ToastUtils.showToast(this, "Coming soon");
                }
                break;
        }
    }

    private void setValues() {
        for (int a = 0; a < addToCartModelList.size(); a++) {
            addToCartResponse = new AddToCartResponse();
            addToCartResponse.setItemCd(addToCartModelList.get(a).getProductId());
            addToCartResponse.setPricePerUnit(addToCartModelList.get(a).getProductAmount() + "");
            addToCartResponse.setOrderCount(addToCartModelList.get(a).getProductQuantity());
            addToCartResponse.setName(BiteRitePreference.getInstance().getUserName());
            addToCartResponse.setMobileNumber(BiteRitePreference.getInstance().getPhoneNo());
            addToCartResponse.setRestaurantId(addToCartModelList.get(a).getRestaurentId() + "");
            addToCartResponse.setPaymentType("COD");
            addToCartResponse.setAddress(address);
            addToCartResponseList.add(a,addToCartResponse);
        }
        hitApiRequest(ApiConstants.REQUEST_MOBILE_ORDER_LIST);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        try {

            VolleyJsonRequest request;
            Class className;
            switch (reqType) {

                case ApiConstants.REQUEST_MOBILE_ORDER_LIST:
                    ((BaseActivity) this).showProgressDialog();
                    String url = ApiConstants.URL_MOBILE_ORDER_LIST;
                    className = CodResponse.class;
                    request = VolleyJsonRequest.doPost(url, new UpdateJsonListener(this, this, reqType, className) {
                    }, getParams(reqType));
                    ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                    break;

                default:
                    hitApiRequest(reqType);
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getParams(int reqType) {
        if (reqType == ApiConstants.REQUEST_MOBILE_ORDER_LIST) {
            AddToCartListResponse addToCartListResponse = new AddToCartListResponse();
            addToCartListResponse.setAddToCartResponses(addToCartResponseList);
            return new Gson().toJson(addToCartListResponse);
        }
        return null;
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        if (this == null) {
            return;
        }
        ((BaseActivity) this).removeProgressDialog();
        try {
            if (!isSuccess) {
                Toast.makeText(getApplicationContext(), "Some error occured", Toast.LENGTH_SHORT).show();
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_MOBILE_ORDER_LIST:
                    CodResponse codResponse = (CodResponse) responseObject;
                    sendUpdate(codResponse);
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void sendUpdate(CodResponse codResponse) {

        if (codResponse.getStatusCode() == 3001) {
            removeAllItemToCart();
            Intent intent = new Intent(this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            ToastUtils.showToast(this, codResponse.getStatusMessage());
        } else {
            ToastUtils.showToast(this, codResponse.getStatusMessage());
        }

    }
}
