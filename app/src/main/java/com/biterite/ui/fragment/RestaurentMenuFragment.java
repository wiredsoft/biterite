package com.biterite.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.RestaurentMenuResponse;
import com.biterite.ui.adapter.RestaurentMenuAdapter;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jyoti goyal on 19-10-2016.
 */

public class RestaurentMenuFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private RestaurentMenuAdapter restaurentMenuAdapter;
    private List<RestaurentMenuResponse.Productlist> restaurentResponseList = new ArrayList<>();
    private int pos;
    private String code;
    private View mView;

    @Override
    public void setHeader() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_restaurent_menu, null);

        recyclerView = (RecyclerView) mView.findViewById(R.id.recylerViewMenu);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        restaurentMenuAdapter = new RestaurentMenuAdapter(getActivity(), this);
        recyclerView.setAdapter(restaurentMenuAdapter);
        hitApiRequest(ApiConstants.REQUEST_RESTAURENT_LIST);

        return mView;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_RESTAURENT_LIST:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_RESTAURENT_LIST;
                className = RestaurentMenuResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;

            }
            switch (reqType) {
                case ApiConstants.REQUEST_RESTAURENT_LIST:
                    RestaurentMenuResponse restaurentResponse = (RestaurentMenuResponse) responseObject;

                    if (restaurentResponse.getStatusCode() == 1001) {
                        restaurentResponseList = restaurentResponse.getProductlist();
                        setUI(restaurentResponseList);
                    } else {
                        ToastUtils.showToast(getActivity(), "Something went wrong");
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void setUI(List<RestaurentMenuResponse.Productlist> restaurentResponseList) {
        restaurentMenuAdapter.setListData(restaurentResponseList);
        restaurentMenuAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.txtResMenu:
                int pos = (int) view.getTag();
                code = restaurentResponseList.get(pos).getProductTypeCode();
                Bundle bundle = new Bundle();
                bundle.putString("productTypeCode", code);
                bundle.putString("resturent_id", getArguments().getString("resturent_id"));

              /*  if (pos == 0) {
                    bundle.putString("category", "N");
                } else if (pos == 1) {
                    bundle.putString("category", "S");
                } else if (pos == 2) {
                    bundle.putString("category", "P");
                }*/
                ((BaseActivity) getActivity()).replaceFragmentTwo(RestaurentMenuProductFragment.class.getSimpleName(), new RestaurentMenuProductFragment(), bundle, true);


                break;
        }
    }

}
