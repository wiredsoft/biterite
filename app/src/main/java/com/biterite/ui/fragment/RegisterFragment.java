package com.biterite.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.constants.AppConstants;
import com.biterite.model.RegisterResponse;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;

/**
 * Created by Amit Kumar Mandal on 11/7/2016.
 */
public class RegisterFragment extends BaseFragment {
    View mView;
    private EditText edtName, edtNumber, edtEmail, edtPassword, edtConfirmPassword;
    private Button btnRegister;
    String url;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_register, null);

        edtName = (EditText) mView.findViewById(R.id.edtName);
        edtNumber = (EditText) mView.findViewById(R.id.edtNumber);
        edtEmail = (EditText) mView.findViewById(R.id.edtEmail);
        edtPassword = (EditText) mView.findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText) mView.findViewById(R.id.edtConfirmPassword);
        btnRegister = (Button) mView.findViewById(R.id.register);
        btnRegister.setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                if (validateData()) {
                    if (((CheckBox) mView.findViewById(R.id.checkboxAgree)).isChecked()) {
                        hitApiRequest(ApiConstants.REQUEST_REGISTER);
                    } else {
                        ToastUtils.showToast(getActivity(), "Please check terms and condition");
                    }
                }
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtName.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter name.");
            return false;
        } else if (!StringUtils.isValidMobileNumber(edtNumber.getText().toString(), false, 10)) {
            ToastUtils.showToast(getActivity(), "Please enter valid mobile number");
            return false;
        } else if (!StringUtils.isValidEmail(edtEmail.getText().toString(), false)) {
            ToastUtils.showToast(getActivity(), "Please enter valid Email.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtConfirmPassword.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter Password");
            return false;
        } else if (!edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Confirm password do not match");
            return false;
        }
        return true;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_REGISTER:
                url = ApiConstants.URL_REGISTER;
                className = RegisterResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_REGISTER) {
            params.put(ApiConstants.PARAMS.USERNAME, edtName.getText().toString());
            params.put(ApiConstants.PARAMS.MOBILE, edtNumber.getText().toString());
            params.put(ApiConstants.PARAMS.EMAILID, edtEmail.getText().toString());
            params.put(ApiConstants.PARAMS.PASSWORD1, edtConfirmPassword.getText().toString());
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_REGISTER:
                    RegisterResponse registerResponse = (RegisterResponse) responseObject;
                    if (registerResponse.getStatusCode() == 4001) {
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.EXTRA_PHONE_NO, edtNumber.getText().toString());
                        bundle.putString(AppConstants.EXTRA_USERNAME, edtName.getText().toString());
                        bundle.putString("password", edtConfirmPassword.getText().toString());
                        bundle.putString("email", edtEmail.getText().toString());
                        bundle.putBoolean(AppConstants.FROM_TROUBLE, false);
                        BiteRitePreference.getInstance().setUserId(registerResponse.getUserid() + "");
                        ((BaseActivity) getActivity()).replaceFragment(RegisterFragment.class.getSimpleName(), new OtpFragment(), bundle, true);
                    } else if (registerResponse.getStatusCode() == 4003) {
                        ToastUtils.showToast(getActivity(), registerResponse.getStatusMessage());
                    } else {
                        ToastUtils.showToast(getActivity(), registerResponse.getStatusMessage());
                    }
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
