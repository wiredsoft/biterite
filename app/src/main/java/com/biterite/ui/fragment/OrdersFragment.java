package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.PreviousOrderResponse;
import com.biterite.ui.activity.HomeActivity;
import com.biterite.ui.activity.OrderStatus;
import com.biterite.ui.adapter.PreviousOrdersAdapter;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saurabh Vardani on 05-11-2016.
 */

public class OrdersFragment extends BaseFragment {
    private View mView;
    private Button btnFindRest;
    private RecyclerView listView;
    private RelativeLayout relNoOrders, relOrders;
    private PreviousOrdersAdapter previousOrdersAdapter;
    List<PreviousOrderResponse.ObjUserOrder> objUserOrders = new ArrayList<>();

    @Override
    public void setHeader() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_orders, null);

        BiteRitePreference.getInstance().setShowDilogorNot(false);

        relNoOrders = (RelativeLayout) mView.findViewById(R.id.relNoOrders);
        relOrders = (RelativeLayout) mView.findViewById(R.id.relOrders);
        listView = (RecyclerView) mView.findViewById(R.id.listViewOrders);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);

        previousOrdersAdapter = new PreviousOrdersAdapter(getActivity(), this);
        listView.setAdapter(previousOrdersAdapter);

        hitApiRequest(ApiConstants.REQUEST_PREVIOUS_ORDER_LIST);
        btnFindRest = (Button) mView.findViewById(R.id.btnFindRest);
        btnFindRest.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnFindRest:
                ((BaseActivity) getActivity()).replaceFragment(null, new RestaurantFragment(), null, false);
                ((HomeActivity) getActivity()).ChangeColor(0);
                break;
            case R.id.relRestaurentList:
                int pos = (int) view.getTag();
                Intent intent = new Intent(getActivity(), OrderStatus.class);
                intent.putExtra("position", String.valueOf(pos));
                startActivity(intent);
                break;
            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_PREVIOUS_ORDER_LIST:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_PREVIOUS_ORDERS_LIST + BiteRitePreference.getInstance().getPhoneNo();
                className = PreviousOrderResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                hitApiRequest(reqType);
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        if (this == null) {
            return;
        }
        ((BaseActivity) getActivity()).removeProgressDialog();

        try {
            if (!isSuccess) {
                Toast.makeText(getActivity(), "Some error occured", Toast.LENGTH_SHORT).show();
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_PREVIOUS_ORDER_LIST:
                    ((BaseActivity) getActivity()).removeProgressDialog();
                    PreviousOrderResponse previousOrderResponse = (PreviousOrderResponse) responseObject;
                    if (previousOrderResponse.getStatusCode() == 1101) {
                        for (int i = 0; i < previousOrderResponse.getObjUserOrder().size(); i++) {
                            objUserOrders.add(previousOrderResponse.getObjUserOrder().get(i));
                        }
                        previousOrdersAdapter.setListData(objUserOrders);
                        previousOrdersAdapter.notifyDataSetChanged();
                        if (previousOrderResponse.getObjUserOrder().size() < 1) {
                            relNoOrders.setVisibility(View.VISIBLE);
                            relOrders.setVisibility(View.GONE);
                        } else {
                            relNoOrders.setVisibility(View.GONE);
                            relOrders.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong !", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}