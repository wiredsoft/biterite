package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.LoginResponse;
import com.biterite.ui.activity.DeliveryTypeActivity;
import com.biterite.utils.BiteRitePreference;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by kunal on 11/7/2016.
 */
public class LoginAfterCartFragment extends BaseFragment {
    private View mView;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    private TextView txtRegister, txtLoginScreen;
    private EditText edtMobileNo, edtPassword;
    private Button btnLogin;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FacebookSdk.sdkInitialize(this.getActivity());
        callbackManager = CallbackManager.Factory.create();

        mView = inflater.inflate(R.layout.fragment_login_after_cart, null);


        edtMobileNo = (EditText) mView.findViewById(R.id.edtMobileNo);
        edtPassword = (EditText) mView.findViewById(R.id.edtPassword);

        loginButton = (LoginButton) mView.findViewById(R.id.txtFblogin);
        txtLoginScreen = (TextView) mView.findViewById(R.id.txtLoginScreen);
        txtRegister = (TextView) mView.findViewById(R.id.txtRegister);

        txtLoginScreen.setOnClickListener(this);
        txtRegister.setOnClickListener(this);
        loginButton.setOnClickListener(this);

        return mView;


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtLoginScreen:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_LOGIN);
                }
                break;

            case R.id.txtRegister:

                ((BaseActivity) getActivity()).replaceFragment(LoginAfterCartFragment.class.getSimpleName(), new RegisterFragment(), null, true);

                break;

//            case R.id.txtGPlusLogin:
//                signInThroughGoogle();
//                break;

            case R.id.txtFblogin:
                onFbLogin();
                break;

            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_LOGIN:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_LOGIN;
                className = LoginResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                hitApiRequest(reqType);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();

        if (reqType == ApiConstants.REQUEST_LOGIN) {
            params.put(ApiConstants.PARAMS.USER_NAME, edtMobileNo.getText().toString());
            params.put(ApiConstants.PARAMS.PASSWORD, edtPassword.getText().toString());

        }
        return params;


    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        if (this == null) {
            return;
        }
        ((BaseActivity) getActivity()).removeProgressDialog();

        try {
            if (!isSuccess) {
                Toast.makeText(getActivity(), "Some error occured", Toast.LENGTH_SHORT).show();
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN:

                    LoginResponse loginResponse = (LoginResponse) responseObject;
                    if (loginResponse.getStatus() == 6001) {
                        BiteRitePreference.getInstance().setLoggedIn(true);
                        BiteRitePreference.getInstance().setUserId(loginResponse.getUserID() + "");
                        BiteRitePreference.getInstance().setUserName(loginResponse.getUserName());
                        BiteRitePreference.getInstance().setEmail(loginResponse.getEmailID());
                        BiteRitePreference.getInstance().setPhoneNo(edtMobileNo.getText().toString());
                        Intent intent = new Intent(getActivity(), DeliveryTypeActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    } else {
                        Toast.makeText(getActivity(), "wrong username and password", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtMobileNo.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter Mobile Number");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter password");
            return false;
        }
        return true;
    }


    private void onFbLogin() {

        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends,user_location"));

        loginButton.setFragment(this);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            private String string;

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                try {
                                    String email = object.getString("email");
                                    String birthday = object.getString("birthday");
                                    String id = object.getString("id");
                                    String name = object.getString("name");

                                    JSONObject object1 = object.getJSONObject("location");
                                    String loc = object1.getString("name");


                                    BiteRitePreference.getInstance().setLoggedIn(true);
                                    BiteRitePreference.getInstance().setFbLoggedIn(true);
                                    BiteRitePreference.getInstance().setUserId(id);
                                    BiteRitePreference.getInstance().setEmail(email);
                                    BiteRitePreference.getInstance().setUserName(name);
                                    Intent intent = new Intent(getActivity(), DeliveryTypeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });


                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,location");
                request.setParameters(parameters);
                request.executeAsync();


                accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                               AccessToken currentAccessToken) {
                    }
                };
            }

            @Override
            public void onCancel() {
                ToastUtils.showToast(getActivity(), "cancel");

            }

            @Override
            public void onError(FacebookException error) {
                ToastUtils.showToast(getActivity(), error.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            //Calling a new function to handle signin
//            handleSignInResult(result);
        // }

    }


}
