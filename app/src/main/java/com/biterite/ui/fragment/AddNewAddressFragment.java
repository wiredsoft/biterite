package com.biterite.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.AddressResponse;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;

/**
 * Created by Saurabh Vardani on 07-11-2016.
 */

public class AddNewAddressFragment extends BaseFragment {
    private EditText edtCity, edtLocation, edtFlat, edtApartment, edtPostcode, edtState;
    private Button btnCreateAddress;
    String address;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_add_new_address, null);

        edtCity = (EditText) mView.findViewById(R.id.edtCity);
        edtLocation = (EditText) mView.findViewById(R.id.edtLocation);
        edtFlat = (EditText) mView.findViewById(R.id.edtFlat);
        edtApartment = (EditText) mView.findViewById(R.id.edtApartment);
        edtPostcode = (EditText) mView.findViewById(R.id.edtPostcode);
        edtState = (EditText) mView.findViewById(R.id.edtState);

        btnCreateAddress = (Button) mView.findViewById(R.id.btnCreateAddress);
        btnCreateAddress.setOnClickListener(this);
        return mView;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_ADD_ADDRESS:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_ADD_ADDRESS;
                className = AddressResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();

        if (reqType == ApiConstants.REQUEST_ADD_ADDRESS) {
            params.put(ApiConstants.PARAMS.USER_ID_ADDRESS, BiteRitePreference.getInstance().getUserId());
            params.put(ApiConstants.PARAMS.STATE_ADDRESS, edtState.getText().toString());
            params.put(ApiConstants.PARAMS.CITY_ADDRESS, edtCity.getText().toString());
            params.put(ApiConstants.PARAMS.LOCATION_ADDRESS, edtLocation.getText().toString());
            params.put(ApiConstants.PARAMS.ADDRESS_ADDRESS, address);
            params.put(ApiConstants.PARAMS.PINCODE_ADDRESS, edtPostcode.getText().toString());
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;

            }
            switch (reqType) {
                case ApiConstants.REQUEST_ADD_ADDRESS:
                    AddressResponse addressResponse = (AddressResponse) responseObject;

                    if (addressResponse.getStatusCode() == 7001) {
                        ToastUtils.showToast(getActivity(), addressResponse.getStatusMessage());
                        ((BaseActivity) getActivity()).replaceFragmentTwo(null, new AddressListFragment(), null, false);
                    } else {
                        ToastUtils.showToast(getActivity(), "Something went wrong");
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setHeader() {

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnCreateAddress:
                if (validateData()) {
                    address = edtFlat.getText().toString() + ", " + edtApartment.getText().toString();
                    hitApiRequest(ApiConstants.REQUEST_ADD_ADDRESS);
                }
                break;
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtFlat.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter Flat No. / House No.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtApartment.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter Apartment / Locality Name");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtState.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter State");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtCity.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter City");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtLocation.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter Location");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPostcode.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter PostCode");
            return false;
        }
        return true;
    }
}
