package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.ui.activity.LoginActivity;
import com.biterite.ui.activity.RegisterActivity;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by K.Agg on 18-10-2016.
 */
public class LoginChooseFragment extends BaseFragment {
    //    private GoogleSignInOptions gso;
//    private GoogleApiClient mGoogleApiClient;
//    private int RC_SIGN_IN = 100;
    private View mView;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    private TextView txtLogin, txtCreate, txtGPlusLogin;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        mView = inflater.inflate(R.layout.fragment_home_screen, null);
        loginButton = (LoginButton) mView.findViewById(R.id.txtFblogin);
        txtLogin = (TextView) mView.findViewById(R.id.txtLogin);
        txtCreate = (TextView) mView.findViewById(R.id.txtCreate);

        txtLogin.setOnClickListener(this);
        txtCreate.setOnClickListener(this);
        loginButton.setOnClickListener(this);

        return mView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtLogin:
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                break;

            case R.id.txtCreate:
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivity(intent);
                break;

//            case R.id.txtGPlusLogin:
//                signInThroughGoogle();
//                break;

            case R.id.txtFblogin:
                onFbLogin();
                break;

            default:
                super.onClick(view);
        }
    }

    private void onFbLogin() {

        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends,user_location"));

        loginButton.setFragment(this);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            private String string;

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                try {


                                    if (!object.toString().contains("email")) {
                                        FacebookSdk.sdkInitialize(getActivity());
                                        LoginManager.getInstance().logOut();
                                        loginButton.performClick();
                                    } else {
                                        String email = object.getString("email");
                                    }


                                   /* String email = object.getString("email");
                                    String birthday = object.getString("birthday");
                                    String id = object.getString("id");
                                    String name = object.getString("name");

                                    JSONObject object1 = object.getJSONObject("location");
                                    String loc = object1.getString("name");*/



                                 /*   BiteRitePreference.getInstance().setLoggedIn(true);
                                    BiteRitePreference.getInstance().setFbLoggedIn(true);
                                    BiteRitePreference.getInstance().setUserId(id);
                                    BiteRitePreference.getInstance().setEmail(email);
                                    BiteRitePreference.getInstance().setUserName(name);
                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);*/


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });


                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,location");
                request.setParameters(parameters);
                request.executeAsync();


                accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                               AccessToken currentAccessToken) {
                    }
                };
            }

            @Override
            public void onCancel() {
                ToastUtils.showToast(getActivity(), "cancel");

            }

            @Override
            public void onError(FacebookException error) {
                ToastUtils.showToast(getActivity(), error.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            //Calling a new function to handle signin
//            handleSignInResult(result);
        // }

    }

//    private void signInThroughGoogle() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }


//    //After the signing we are calling this function
//    private void handleSignInResult(GoogleSignInResult result) {
//        //If the login succeed
//        if (result.isSuccess()) {
//            //Getting google account
//            GoogleSignInAccount acct = result.getSignInAccount();
//            BiteRitePreference.getInstance().setLoggedIn(true);
//            BiteRitePreference.getInstance().setFbLoggedIn(false);
//            BiteRitePreference.getInstance().setGoogleLoggedIn(true);
//            BiteRitePreference.getInstance().setUserId(acct.getId() + "");
//            BiteRitePreference.getInstance().setUserName(acct.getDisplayName());
//            BiteRitePreference.getInstance().setPhoneNo(acct.getEmail());
//            Intent intent = new Intent(getActivity(), HomeActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//
//
//        } else {
//
//            ToastUtils.showToast(getActivity(), "Login Failed");
//        }
//    }
//
//    @Override
//    public void onConnected(@Nullable Bundle bundle) {
//
//    }
//
//    @Override
//    public void onConnectionSuspended(int i) {
//
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
}