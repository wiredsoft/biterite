package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.constants.AppConstants;
import com.biterite.model.OTPResponse;
import com.biterite.ui.activity.DeliveryTypeActivity;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;

import java.util.HashMap;

/**
 * Created by Amit Kumar Mandal on 11/7/2016.
 */
public class OtpFragment extends BaseFragment {

    private String url;
    private String phoneNumber, userName, password, email;
    private EditText edtotp;


    View mView;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_otp_verify, null);

        phoneNumber = getArguments().getString(AppConstants.EXTRA_PHONE_NO);
        userName = getArguments().getString(AppConstants.EXTRA_USERNAME);
        password = getArguments().getString("password");
        email = getArguments().getString("email");
        edtotp = (EditText) mView.findViewById(R.id.edtotp);

        mView.findViewById(R.id.txtSubmit).setOnClickListener(this);


        return mView;
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtotp.getText().toString())) {
            ToastUtils.showToast(getActivity(), "Please enter otp.");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSubmit:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_VERIFY_OTP);
                }
                break;
            default:
                super.onClick(view);
        }
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_VERIFY_OTP:
                url = ApiConstants.URL_VERIFY_OTP;
                className = OTPResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_VERIFY_OTP) {
            params.put(ApiConstants.PARAMS.USER_ID_OTP, BiteRitePreference.getInstance().getUserId());
            params.put(ApiConstants.PARAMS.OTP, edtotp.getText().toString().trim());
            params.put(ApiConstants.PARAMS.EMAILID, email);
            params.put(ApiConstants.PARAMS.PASSWORD1, password);
            /*params.put(ApiConstants.PARAMS.PASSWORD1, StringUtils.getBase64String(edtConfirmPassword.getText().toString()));*/
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_VERIFY_OTP:
                    OTPResponse OTPResponse = (OTPResponse) responseObject;
                    if (OTPResponse.getStatusCode().equals("5001")) {
                        BiteRitePreference.getInstance().setLoggedIn(true);
                        BiteRitePreference.getInstance().setUserName(userName);
                        BiteRitePreference.getInstance().setEmail(email);
                        BiteRitePreference.getInstance().setPhoneNo(phoneNumber);
                        AlertDialogUtils.showAlertDialog(getActivity(), "BiteRite", "Your login credentials are in your message inbox :)", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(getActivity(), DeliveryTypeActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }
                        });

                    } else {
                        ToastUtils.showToast(getActivity(), "Wrong OTP");
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
