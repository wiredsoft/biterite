package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.AddressListResponse;
import com.biterite.ui.activity.DoPaymentActivity;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saurabh Vardani on 07-11-2016.
 */

public class ShowAddressFragment extends BaseFragment {
    private TextView txtChangeAddress, txtDeliveryAddress, txtTelephoneNumber;
    private Button btnSelectAddress;
    List<AddressListResponse.UserAddresslist> addressListResponsesList = new ArrayList<>();
    String address;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_show_address, null);
        txtChangeAddress = (TextView) mView.findViewById(R.id.txtChangeAddress);
        txtDeliveryAddress = (TextView) mView.findViewById(R.id.txtDeliveryAddress);
        txtTelephoneNumber = (TextView) mView.findViewById(R.id.txtTelephoneNumber);
        btnSelectAddress = (Button) mView.findViewById(R.id.btnSelectAddress);
        Bundle bundle = getArguments();
        if (bundle != null)
            address = bundle.getString("address");
        if (address != null) {
            txtDeliveryAddress.setText(address);
            txtTelephoneNumber.setText(BiteRitePreference.getInstance().getPhoneNo());
        } else {
            hitApiRequest(ApiConstants.REQUEST_LIST_ADDRESS);
        }

        txtChangeAddress.setOnClickListener(this);
        btnSelectAddress.setOnClickListener(this);
        return mView;
    }

    @Override
    public void setHeader() {

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.txtChangeAddress:
                ((BaseActivity) getActivity()).replaceFragmentTwo(ShowAddressFragment.class.getSimpleName(), new AddressListFragment(), null, true);
                break;
            case R.id.btnSelectAddress:
                Intent intent1 = new Intent(getActivity(), DoPaymentActivity.class);
                intent1.putExtra("address", address);
                startActivity(intent1);
                break;
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_LIST_ADDRESS:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_LIST_ADDRESS + BiteRitePreference.getInstance().getUserId();
                className = AddressListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;

            }
            switch (reqType) {
                case ApiConstants.REQUEST_LIST_ADDRESS:
                    AddressListResponse addressListResponse = (AddressListResponse) responseObject;
                    if (addressListResponse.getStatusCode() == 8001) {
                        address = addressListResponse.getUserAddresslist().get(0).getName() + "\nAddress :  " + addressListResponse.getUserAddresslist().get(0).getAddress() + "\nLocation :  " + addressListResponse.getUserAddresslist().get(0).getLocation() + "\nCity :  " + addressListResponse.getUserAddresslist().get(0).getCity() + "\nState :  " + addressListResponse.getUserAddresslist().get(0).getState() + "\nPinCode :  " + addressListResponse.getUserAddresslist().get(0).getPinCode();
                        txtDeliveryAddress.setText(address);
                        txtTelephoneNumber.setText(BiteRitePreference.getInstance().getPhoneNo());
                    } else {
                        ToastUtils.showToast(getActivity(), addressListResponse.getStatusMessage());
                        ((BaseActivity) getActivity()).replaceFragmentTwo(ShowAddressFragment.class.getSimpleName(), new AddNewAddressFragment(), null, true);
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
