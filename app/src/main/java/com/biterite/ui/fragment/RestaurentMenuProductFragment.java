package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.RestaurentProductMenuResponse;
import com.biterite.ui.activity.MenuDetailsActivity;
import com.biterite.ui.adapter.RestaurentProductMenuAdapter;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jyoti goyal on 19-10-2016.
 */

public class RestaurentMenuProductFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private RestaurentProductMenuAdapter restaurentMenuAdapter;
    private List<RestaurentProductMenuResponse.ProductlstbycatID> restaurentProductResponseList = new ArrayList<>();
    private View mView;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_restaurent_menu_product_list, null);
        recyclerView = (RecyclerView) mView.findViewById(R.id.listView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        restaurentMenuAdapter = new RestaurentProductMenuAdapter(getActivity(), this);
        recyclerView.setAdapter(restaurentMenuAdapter);
        hitApiRequest(ApiConstants.REQUEST_RESTAURENT_MENU_LIST);

        return mView;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_RESTAURENT_MENU_LIST:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_RESTAURENT_MENU_LIST + getArguments().getString("productTypeCode");
                className = RestaurentProductMenuResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_RESTAURENT_MENU_LIST:
                    RestaurentProductMenuResponse restaurentMenuResponse = (RestaurentProductMenuResponse) responseObject;
                    if (restaurentMenuResponse.getStatusCode() == 2001) {
                        restaurentProductResponseList = restaurentMenuResponse.getProductlstbycatID();
                        setUi(restaurentProductResponseList);
                    } else {
                        ToastUtils.showToast(getActivity(), "Something went wrong");
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setUi(List<RestaurentProductMenuResponse.ProductlstbycatID> restaurentProductResponseList) {
        restaurentMenuAdapter.setListData(restaurentProductResponseList);
        restaurentMenuAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLinear:
                int pos = (int) view.getTag();
                Intent intent = new Intent(getActivity(), MenuDetailsActivity.class);
                intent.putExtra("productName", restaurentProductResponseList.get(pos).getProductName());
                intent.putExtra("productPrice", restaurentProductResponseList.get(pos).getProductRate());
                intent.putExtra("productType", restaurentProductResponseList.get(pos).getProductType());
                intent.putExtra("productCode", restaurentProductResponseList.get(pos).getProductCode());
                intent.putExtra("productTypeCode", getArguments().getString("productTypeCode"));
                intent.putExtra("resturent_id", getArguments().getString("resturent_id"));
                startActivity(intent);

                break;

            default:
                super.onClick(view);
        }

    }

}

