package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.ObjectRestaurent;
import com.biterite.model.RestaurentListModel;
import com.biterite.model.RestaurentMenuResponse;
import com.biterite.ui.activity.RestaurentListActivity;
import com.biterite.ui.adapter.MyViewPagerAdapter;
import com.biterite.ui.adapter.RestaurentListAdapter;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.List;
import java.util.Timer;

/**
 * Created by K.Agg on 18-10-2016.
 */
public class RestaurantFragment extends BaseFragment implements TextWatcher {
    List<ObjectRestaurent> restaurentResponseList;

    private View mView;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private RestaurentListAdapter restaurentListAdapter;
    private RecyclerView listView;
    Timer timer;
    int page = 1;


    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_restaurent_list, null);
        BiteRitePreference.getInstance().setShowDilogorNot(true);
        ((EditText) mView.findViewById(R.id.edtSearch)).addTextChangedListener(this);
        viewPager = (ViewPager) mView.findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) mView.findViewById(R.id.layoutDots);

        listView = (RecyclerView) mView.findViewById(R.id.listView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);
        restaurentListAdapter = new RestaurentListAdapter(getActivity(), this);
        listView.setAdapter(restaurentListAdapter);

        layouts = new int[]
                {
                        R.layout.view_img,
                        R.layout.view_img
                };
        // adding bottom dots
        addBottomDots(0);


        myViewPagerAdapter = new MyViewPagerAdapter();
        myViewPagerAdapter.setLayout(layouts, getActivity());
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


        hitApiRequest(ApiConstants.REQUEST_RESTAURENT);

        //timer


        return mView;
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_RESTAURENT:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_RESTAURENT;
                className = RestaurentListModel.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;

            }
            switch (reqType) {
                case ApiConstants.REQUEST_RESTAURENT:
                    RestaurentListModel restaurentResponse = (RestaurentListModel) responseObject;

                    if (restaurentResponse.getStatusCode() == 1103) {
                        restaurentResponseList = restaurentResponse.getObjOutList();
                        setUI(restaurentResponseList);
                    } else {
                        ToastUtils.showToast(getActivity(), "Something went wrong");
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setUI(List<ObjectRestaurent> restaurentResponseList) {
        restaurentListAdapter.setList(restaurentResponseList);
        restaurentListAdapter.notifyDataSetChanged();
    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getActivity());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    //  viewpager change listener

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }


        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relRestaurentList:
                int pos = (int) view.getTag();
                Intent intent = new Intent(getActivity(), RestaurentListActivity.class);
                intent.putExtra("resturent_id", restaurentResponseList.get(pos).getOutLetCode());
                startActivity(intent);
                break;
            default:
                super.onClick(view);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() > 1) {


        } else {

        }
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}








