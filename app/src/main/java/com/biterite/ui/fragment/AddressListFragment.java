package com.biterite.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.biterite.R;
import com.biterite.constants.ApiConstants;
import com.biterite.model.AddressListResponse;
import com.biterite.ui.adapter.AddressListAdapter;
import com.biterite.utils.BiteRitePreference;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saurabh Vardani on 07-11-2016.
 */

public class AddressListFragment extends BaseFragment {
    private Button btnAddNewAddress;
    private RecyclerView listView;
    private AddressListAdapter addressListAdapter;
    List<AddressListResponse.UserAddresslist> addressListResponsesList = new ArrayList<>();
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_address_list, null);
        listView = (RecyclerView) mView.findViewById(R.id.listView);
        btnAddNewAddress = (Button) mView.findViewById(R.id.btnAddNewAddress);
        btnAddNewAddress.setOnClickListener(this);
        hitApiRequest(ApiConstants.REQUEST_LIST_ADDRESS);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(linearLayoutManager);
        addressListAdapter = new AddressListAdapter(getActivity(), this);
        listView.setAdapter(addressListAdapter);
        return mView;
    }


    @Override
    public void setHeader() {

    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_LIST_ADDRESS:
                ((BaseActivity) getActivity()).showProgressDialog();
                String url = ApiConstants.URL_LIST_ADDRESS + BiteRitePreference.getInstance().getUserId();
                className = AddressListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        ((BaseActivity) getActivity()).removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;

            }
            switch (reqType) {
                case ApiConstants.REQUEST_LIST_ADDRESS:
                    AddressListResponse addressListResponse = (AddressListResponse) responseObject;

                    if (addressListResponse.getStatusCode() == 8001) {
                        for (int i = 0; i < addressListResponse.getUserAddresslist().size(); i++) {
                            addressListResponsesList.add(addressListResponse.getUserAddresslist().get(i));
                        }
                        addressListAdapter.setListData(addressListResponsesList);
                        addressListAdapter.notifyDataSetChanged();
                    } else {
                        ToastUtils.showToast(getActivity(), addressListResponse.getStatusMessage());
                        ((BaseActivity) getActivity()).replaceFragmentTwo(AddressListFragment.class.getSimpleName(), new AddNewAddressFragment(), null, true);
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnAddNewAddress:
                ((BaseActivity) getActivity()).replaceFragmentTwo(AddressListFragment.class.getSimpleName(), new AddNewAddressFragment(), null, true);
                break;

            case R.id.txtAddress:
                int pos = (int) view.getTag();
                String address = addressListResponsesList.get(pos).getName() + "\nAddress :  " + addressListResponsesList.get(pos).getAddress() + "\nLocation :  " + addressListResponsesList.get(pos).getLocation() + "\nCity :  " + addressListResponsesList.get(pos).getCity() + "\nState :  " + addressListResponsesList.get(pos).getState() + "\nPinCode :  " + addressListResponsesList.get(pos).getPinCode();
                Bundle args = new Bundle();
                args.putString("address", address);
                ((BaseActivity) getActivity()).replaceFragmentTwo(null, new ShowAddressFragment(), args, false);
                ((BaseActivity) getActivity()).getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                break;
        }
    }
}
