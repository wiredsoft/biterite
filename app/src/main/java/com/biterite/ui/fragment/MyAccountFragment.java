package com.biterite.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.ui.activity.ChangePasswordActivity;
import com.biterite.ui.activity.HomeActivity;
import com.biterite.utils.BiteRitePreference;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;

/**
 * Created by kunal on 10/19/2016.
 */
public class MyAccountFragment extends BaseFragment {

    private View mView;
    EditText edtFirstName, edtEmail, edtMobile;
    TextView btnChangePass;
//    private GoogleApiClient mGoogleApiClient;
//    private GoogleSignInOptions gso;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_myaccount, null);
        BiteRitePreference.getInstance().setShowDilogorNot(false);
        setHasOptionsMenu(true);
        edtFirstName = (EditText) mView.findViewById(R.id.edtFirstName);
        edtEmail = (EditText) mView.findViewById(R.id.edtEmail);
        edtMobile = (EditText) mView.findViewById(R.id.edtMobile);
        btnChangePass = (TextView) mView.findViewById(R.id.btnChangePass);
//        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//        //Initializing google api client
//        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
//                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
        if (BiteRitePreference.getInstance().getLoggedIn() == true) {

            edtFirstName.setText(BiteRitePreference.getInstance().getUserName());
            edtEmail.setText(BiteRitePreference.getInstance().getEmail());

            edtMobile.setText(BiteRitePreference.getInstance().getPhoneNo());
        }


        /*txtEdit.setOnClickListener(this);*/
        btnChangePass.setOnClickListener(this);

        return mView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            /*case R.id.txtEdit:

                txtApply.setVisibility(View.VISIBLE);
                txtEdit.setVisibility(View.GONE);


                edtFirstName.setFocusable(true);
                edtFirstName.setFocusableInTouchMode(true);

                edtLastName.setFocusable(true);
                edtLastName.setFocusableInTouchMode(true);

                edtMobile.setFocusable(true);
                edtMobile.setFocusableInTouchMode(true);

                edtEmail.setFocusable(true);
                edtEmail.setFocusableInTouchMode(true);

                break;*/
            case R.id.btnChangePass:
                Intent i = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(i);
                break;

        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getActivity().getMenuInflater().inflate(R.menu.logout, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.m2:

                if (BiteRitePreference.getInstance().getFbLoggedIn() == true) {

                    AlertDialogUtils.showAlertDialog(getActivity(), "BiteRite", "Do you really want to logout", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {


                            if (AccessToken.getCurrentAccessToken() == null) {
                                return; // already logged out
                            }

                            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                                    .Callback() {
                                @Override
                                public void onCompleted(GraphResponse graphResponse) {

                                    BiteRitePreference.logoutUser();
                                    LoginManager.getInstance().logOut();
                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                }
                            }).executeAsync();
                        }


//                        }else if(BiteRitePreference.getInstance().getGoogleLoggedIn() == true){
//                            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                                    new ResultCallback<Status>() {
//                                        @Override
//                                        public void onResult(Status status) {
//                                            BiteRitePreference.logoutUser();
//                                            Intent intent = new Intent(getActivity(), HomeActivity.class);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                            startActivity(intent);
//                                        }
//                                    });
//                        }


                    });
                    return true;
                } else {
                    BiteRitePreference.logoutUser();
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
        }
        return true;
    }
}
