package com.biterite.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biterite.R;
import com.biterite.ui.adapter.DataAdapter;
import com.biterite.utils.BiteRitePreference;
import com.krapps.ui.BaseFragment;

/**
 * Created by K.Agg on 20-10-2016.
 */
public class DealsFragment extends BaseFragment {

    private View mView;
    private RecyclerView recyclerView;
    private DataAdapter mAdapter;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.activity_recyclerview, null);
        BiteRitePreference.getInstance().setShowDilogorNot(false);

        recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);

        mAdapter = new DataAdapter(getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);


        return mView;
    }

}
