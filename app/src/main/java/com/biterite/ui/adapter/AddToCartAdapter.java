package com.biterite.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.AddToCartModel;
import com.biterite.ui.activity.AddToCartActivity;
import com.biterite.utils.BiteRitePreference;

import java.util.List;

/**
 * Created by Saurabh Vardani on 20-10-2016.
 */

public class AddToCartAdapter extends BaseAdapter {
    private final LayoutInflater mInflator;
    private List<AddToCartModel> listing;
    private View.OnClickListener mClickListener;
    private Context mContext;
    private String[] quantity;
    TextView txt;

    public AddToCartAdapter(Context mContext, String[] quantity, AddToCartActivity addToCartActivity, TextView textView) {
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = mContext;
        this.quantity = quantity;
        this.mClickListener = addToCartActivity;
        this.txt = textView;
    }

    @Override
    public int getCount() {
        return listing == null ? 0 : listing.size();
    }

    @Override
    public Object getItem(int i) {
        return listing.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        final ViewHolder viewholder;
        if (convertView == null) {
            viewholder = new ViewHolder();
            convertView = mInflator.inflate(R.layout.list_add_to_cart_items, null);
            viewholder.txtDishName = (TextView) convertView.findViewById(R.id.txtDishName);
            viewholder.txtAmount = (TextView) convertView.findViewById(R.id.txtAmount);
            viewholder.spnrQuantity = (Spinner) convertView.findViewById(R.id.spnrQuantity);
            viewholder.imgCross = (ImageView) convertView.findViewById(R.id.imgCross);

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }


        viewholder.spnrQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                viewholder.txtAmount.setText("\u20B9" + listing.get(position).getProductAmount() * (i + 1));
                listing.get(position).setProductQuantity(String.valueOf(viewholder.spnrQuantity.getSelectedItem()));
                BiteRitePreference.getInstance().setCart(listing);
                txt.setText("\u20B9" + BiteRitePreference.getInstance().totalCostCart());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        viewholder.txtDishName.setText(listing.get(position).getProductName());
        viewholder.txtAmount.setText(listing.get(position).getProductAmount() + "");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, quantity);
        viewholder.spnrQuantity.setAdapter(adapter);
        viewholder.imgCross.setTag(position);
        viewholder.imgCross.setOnClickListener(mClickListener);

        int Quantity = adapter.getPosition(listing.get(position).getProductQuantity());
        viewholder.spnrQuantity.setSelection(Quantity);

        return convertView;
    }

    public void setListData(List<AddToCartModel> addToCartModels) {
        this.listing = addToCartModels;
    }

    public static class ViewHolder {
        TextView txtDishName;
        TextView txtAmount;
        Spinner spnrQuantity;
        ImageView imgCross;
    }

}

