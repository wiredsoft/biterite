package com.biterite.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.PreviousOrderResponse;
import com.biterite.ui.fragment.OrdersFragment;

import java.util.List;

/**
 * Created by Saurabh Vardani on 14-11-2016.
 */

public class PreviousOrdersAdapter extends RecyclerView.Adapter<PreviousOrdersAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    List<PreviousOrderResponse.ObjUserOrder> objUserOrders;
    private View.OnClickListener mClickListener;
    private String address;

    public PreviousOrdersAdapter(Context context, OrdersFragment ordersFragment) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = ordersFragment;
    }


    @Override
    public PreviousOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_previous_orders, parent, false);
        return new PreviousOrdersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PreviousOrdersAdapter.ViewHolder holder, int position) {
        holder.txtProductName.setText(objUserOrders.get(position).getItemName());
        holder.txtOrderDate.setText(objUserOrders.get(position).getOrderDate());
        holder.relRestaurentList.setTag(position);
        holder.relRestaurentList.setOnClickListener(mClickListener);
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return objUserOrders == null ? 0 : objUserOrders.size();
    }

    public void setListData(List<PreviousOrderResponse.ObjUserOrder> objUserOrders) {
        this.objUserOrders = objUserOrders;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtOrderDate, txtProductName;
        RelativeLayout relRestaurentList;

        public ViewHolder(View itemView) {
            super(itemView);
            txtOrderDate = (TextView) itemView.findViewById(R.id.txtOrdersDate);
            txtProductName = (TextView) itemView.findViewById(R.id.txtProductName);
            relRestaurentList = (RelativeLayout) itemView.findViewById(R.id.relRestaurentList);
        }
    }
}
