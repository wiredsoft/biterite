package com.biterite.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.ObjectRestaurent;
import com.biterite.ui.fragment.RestaurantFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Saurabh Vardani on 27-10-2016.
 */

public class RestaurentListAdapter extends RecyclerView.Adapter<RestaurentListAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private View.OnClickListener mClickListener;
    List<ObjectRestaurent> list;

    public RestaurentListAdapter(Context context, RestaurantFragment restaurantFragment) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = restaurantFragment;
        list = new ArrayList<>();
    }

    public void setList(List<ObjectRestaurent> restaurentResponseList) {
        this.list = restaurentResponseList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_restaurent_items, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtRestraurent.setText(list.get(position).getOutLetName());
        holder.relRestaurentList.setTag(position);
        holder.relRestaurentList.setOnClickListener(mClickListener);
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtRestraurent;
        RelativeLayout relRestaurentList;


        public ViewHolder(View itemView) {
            super(itemView);
            txtRestraurent = (TextView) itemView.findViewById(R.id.txtRestraurent);
            relRestaurentList = (RelativeLayout) itemView.findViewById(R.id.relRestaurentList);
        }
    }
}
