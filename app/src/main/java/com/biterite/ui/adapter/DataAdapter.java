package com.biterite.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biterite.R;

/**
 * Created by kunal on 9/28/2016.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;

    public DataAdapter(Context context) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deals_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        for (position = 0; position < 10; position++) {
            holder.txtOffer.setText("Flat 20% off");
            holder.txtOfferDes.setText("Get 20% off on every order");
            holder.img1.setImageResource(R.drawable.imgfood);
            holder.img2.setImageResource(R.drawable.offer);
        }
    }


    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img1;
        private ImageView img2;
        private TextView txtOffer;
        private TextView txtOfferDes;

        public ViewHolder(View itemView) {
            super(itemView);
            txtOffer = (TextView) itemView.findViewById(R.id.txtOffer);
            txtOfferDes = (TextView) itemView.findViewById(R.id.txtOfferDes);

            img1 = (ImageView) itemView.findViewById(R.id.img1);
            img2 = (ImageView) itemView.findViewById(R.id.img2);

        }
    }
}
