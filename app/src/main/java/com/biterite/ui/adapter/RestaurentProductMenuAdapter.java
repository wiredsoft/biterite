package com.biterite.ui.adapter;

/**
 * Created by Saurabh Vardani on 18-10-2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.RestaurentProductMenuResponse;
import com.biterite.ui.fragment.RestaurentMenuProductFragment;

import java.util.List;


public class RestaurentProductMenuAdapter extends RecyclerView.Adapter<RestaurentProductMenuAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<RestaurentProductMenuResponse.ProductlstbycatID> restaurentProductResponseList;
    private View.OnClickListener mClickListener;

    public RestaurentProductMenuAdapter(Context context, RestaurentMenuProductFragment restaurentMenuActivity) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = restaurentMenuActivity;
    }

    public void setListData(List<RestaurentProductMenuResponse.ProductlstbycatID> restaurentProductResponseList) {
        this.restaurentProductResponseList = restaurentProductResponseList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_restaurent_product_menu, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        viewholder.txtProductName.setText(restaurentProductResponseList.get(position).getProductName());
        viewholder.txtProductPrice.setText("\u20B9 " + restaurentProductResponseList.get(position).getProductRate());
        viewholder.txtProductDescription.setText("Product Descriptions will come soon !");
        viewholder.btnLinear.setOnClickListener(mClickListener);
        viewholder.btnLinear.setTag(position);

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return restaurentProductResponseList == null ? 0 : restaurentProductResponseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtProductName, txtProductPrice, txtProductDescription;
        RelativeLayout btnLinear;


        public ViewHolder(View itemView) {
            super(itemView);
            txtProductName = (TextView) itemView.findViewById(R.id.txtProductName);
            txtProductPrice = (TextView) itemView.findViewById(R.id.txtProductPrice);
            btnLinear = (RelativeLayout) itemView.findViewById(R.id.btnLinear);
            txtProductDescription = (TextView) itemView.findViewById(R.id.txtProductDescription);
        }
    }
}
