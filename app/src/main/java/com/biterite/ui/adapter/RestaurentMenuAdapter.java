package com.biterite.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.RestaurentMenuResponse;
import com.biterite.ui.fragment.RestaurentMenuFragment;

import java.util.List;
import java.util.Random;

/**
 * Created by Saurabh Vardani on 17-10-2016.
 */

public class RestaurentMenuAdapter extends RecyclerView.Adapter<RestaurentMenuAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<RestaurentMenuResponse.Productlist> mlistData;
    private View.OnClickListener mClickListener;

    public RestaurentMenuAdapter(Context context, RestaurentMenuFragment restaurentMenuActivity) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = restaurentMenuActivity;
    }

    public void setListData(List<RestaurentMenuResponse.Productlist> productlists) {
        this.mlistData = productlists;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_restaurent_menu_items, parent, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        if (position == 0) {
            viewholder.imageView.setImageResource(R.drawable.dinner_restro_menu);
        } else if (position == 1) {
            viewholder.imageView.setImageResource(R.drawable.meall);
        } else {
            viewholder.imageView.setImageResource(R.drawable.bf_restro_menu);
        }


        viewholder.txtResMenu.setText(mlistData.get(position).getProductTypeName());
        viewholder.txtResMenu.setTag(position);
        viewholder.txtResMenu.setOnClickListener(mClickListener);
    }

    public int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtResMenu;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            txtResMenu = (TextView) itemView.findViewById(R.id.txtResMenu);
            imageView = (ImageView) itemView.findViewById(R.id.menu_icon);
        }
    }
}
