package com.biterite.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.biterite.R;
import com.biterite.model.AddressListResponse;
import com.biterite.ui.fragment.AddressListFragment;

import java.util.List;

/**
 * Created by Saurabh Vardani on 07-11-2016.
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    List<AddressListResponse.UserAddresslist> userAddresslists;
    private View.OnClickListener mClickListener;
    private String address;

    public AddressListAdapter(Context context, AddressListFragment addressListFragment) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mClickListener = addressListFragment;
    }


    @Override
    public AddressListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_address, parent, false);
        return new AddressListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AddressListAdapter.ViewHolder holder, int position) {
        address = userAddresslists.get(position).getName() + "\nAddress :  " + userAddresslists.get(position).getAddress() + "\nLocation :  " + userAddresslists.get(position).getLocation() + "\nCity :  " + userAddresslists.get(position).getCity() + "\nState :  " + userAddresslists.get(position).getState() + "\nPinCode :  " + userAddresslists.get(position).getPinCode();
        holder.txtAddress.setText(address);
        holder.txtAddress.setTag(position);
        holder.txtAddress.setOnClickListener(mClickListener);
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userAddresslists == null ? 0 : userAddresslists.size();
    }

    public void setListData(List<AddressListResponse.UserAddresslist> addressListResponsesList) {
        this.userAddresslists = addressListResponsesList;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            txtAddress = (TextView) itemView.findViewById(R.id.txtAddress);

        }
    }
}
