package com.biterite.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.biterite.constants.AppConstants;
import com.biterite.model.AddToCartModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by kunal on 10/17/2016.
 */
public class BiteRitePreference {

    private static SharedPreferences mPreferences;
    private static BiteRitePreference mInstance;
    private static SharedPreferences.Editor mEditor;
    private static String IS_LOGGED_IN_FRM_GOOGLE = "is_logged_in_google";
    private static String IS_LOGGED_IN_FRM_FB = "is_logged_in_fb";
    private static String IS_LOGGED_IN = "is_logged_in";
    private static String USER_ID = "user_id";
    private static String USER_NAME = "user_name";
    private static String PHONE_NO = "phone";
    private static String EMAIL = "email";
    private static String CART = "cart";
    private static String SHOW_DIALOG_OR_NOT = "show_dialog_or_not";


    private BiteRitePreference() {

    }

    public static BiteRitePreference getInstance() {
        if (mInstance == null) {
            Context context = BaseApplication.mContext;
            mInstance = new BiteRitePreference();
            mPreferences = context.getSharedPreferences(AppConstants.NIGHT_ADVISOR_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }

    public void setShowDilogorNot(boolean value) {
        mEditor.putBoolean(SHOW_DIALOG_OR_NOT, value).apply();
    }

    public boolean getShowDilogorNot() {
        return mPreferences.getBoolean(SHOW_DIALOG_OR_NOT, false);
    }


    public void setLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN, value).apply();
    }

    public boolean getLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public void setUserId(String value) {
        mEditor.putString(USER_ID, value).apply();
    }

    public String getUserId() {
        return mPreferences.getString(USER_ID, null);
    }

    public void setUserName(String value) {
        mEditor.putString(USER_NAME, value).apply();
    }

    public String getUserName() {
        return mPreferences.getString(USER_NAME, null);
    }

    public void setEmail(String value) {
        mEditor.putString(EMAIL, value).apply();
    }

    public String getEmail() {
        return mPreferences.getString(EMAIL, null);
    }

    public void setPhoneNo(String value) {
        mEditor.putString(PHONE_NO, value).apply();
    }

    public String getPhoneNo() {
        return mPreferences.getString(PHONE_NO, null);
    }

    public static void logoutUser() {
        mEditor.clear();
        mEditor.commit();
    }

    public void setCart(List<AddToCartModel> value) {
        Gson gson = new Gson();
        mEditor.putString(CART, gson.toJson(value)).apply();
    }

    public void setFbLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN_FRM_FB, value).apply();
    }

    public boolean getFbLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN_FRM_FB, false);
    }

    public void setGoogleLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN_FRM_GOOGLE, value).apply();
    }

    public boolean getGoogleLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN_FRM_GOOGLE, false);
    }

    public List<AddToCartModel> getCart() {
        String cart = mPreferences.getString(CART, null);
        Type listType = new TypeToken<List<AddToCartModel>>() {
        }.getType();
        return new Gson().fromJson(cart, listType);
    }

    public float totalCostCart() {
        List<AddToCartModel> addToCartModelslist = BiteRitePreference.getInstance().getCart();
        float total = 0;
        if (addToCartModelslist.size() > 0) {
            for (int i = 0; i < addToCartModelslist.size(); i++) {
                total = total + addToCartModelslist.get(i).getProductAmount() * Float.parseFloat(addToCartModelslist.get(i).getProductQuantity());
            }
        }
        return total;
    }


}
