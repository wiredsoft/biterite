package com.biterite.constants;

public interface ApiConstants {


	interface PARAMS {
		String  USER_NAME               = "username";
		String	USERNAME				= "FirstName";
		String	PASSWORD				= "password";
		String  USER_PASSWORD			="password";
		String  MOBILE					= "mobile";
		String	MOBILE_NUMBER			= "mobileNumber";
		String  EMAILID					= "emailid";
		String  PASSWORD1				= "password";
		String 	USER_ID				    = "userId";
		String	OLDPASS				    = "oldPass";
		String 	NEWPASS		     		= "newPass";
		String  OTP_FORGOT				="otp";
		String  OTP						="OTP";
		String 	USER_ID_OTP			    = "userID";
		String 	USER_ID_ADDRESS			= "userId";
		String 	STATE_ADDRESS			= "state";
		String 	CITY_ADDRESS			= "city";
		String 	LOCATION_ADDRESS		= "location";
		String 	ADDRESS_ADDRESS			= "address";
		String 	PINCODE_ADDRESS			= "pinCode";
		String 	ORDER_COD       		= "MobileOrderList";

	}

	final int		REQUEST_LOGIN					= 1;
	final int		REQUEST_NOTIFICATION_COUNT		= 2;
	final int		REQUEST_RESTAURENT_LIST			= 3;
	final int		REQUEST_RESTAURENT_MENU_LIST	= 4;
	final int		REQUEST_CHECK_NUMBER_REGISTER	= 5;
	final int		REQUEST_REGISTER				= 6;
	final int		REQUEST_VERIFY_OTP				= 7;
	final int 		REQUEST_CHANGE_PASSWORD			= 8;
	final int 		REQUEST_FORGOT_PASSWORD			= 9;
	final int       REQUEST_OTP_VERFICATION			=10;
	final int		REQUEST_ADD_ADDRESS				=11;
	final int		REQUEST_LIST_ADDRESS			=12;
	final int		REQUEST_MOBILE_ORDER_LIST		=13;
	final int		REQUEST_PREVIOUS_ORDER_LIST		=14;
	final int		REQUEST_RESTAURENT       		=15;


	final String	URL_LOGIN					= "http://52.74.127.225/POSAPIS/api/Login";
	final String	URL_RESTAURENT_LIST			= "http://52.74.127.225/posapis/api/GetProduct";
	final String	URL_RESTAURENT_MENU_LIST	= "http://52.74.127.225/posapis/api/GetProductByCategory?Catid=";
	final String	URL_REGISTER				= "http://52.74.127.225/POSAPIS/api/RegisterUser";
	final String    URL_VERIFY_OTP      	    = "http://52.74.127.225/POSAPIS/api/OtpVerificationUser";
	final String    URL_CHANGE_PASSWORD			= "ResetPassword";
	final String    URL_FORGOT_PASSWORD_GET   	= "http://52.74.127.225/POSAPIS/api/ForgotPassword?mobileNo=";
	final String    URL_FORGOT_PASSWORD_POST   	= "http://52.74.127.225/POSAPIS/api/ForgotPassword";
	final String	URL_ADD_ADDRESS			    = "http://52.74.127.225/POSAPIS/api/Address";
	final String	URL_LIST_ADDRESS			= "http://52.74.127.225/POSAPIS/api/Address/GetUserAddress?userId=";
	final String	URL_MOBILE_ORDER_LIST		= "http://52.74.127.225/POSAPIS/api/AddtoCart";
	final String	URL_PREVIOUS_ORDERS_LIST	= "http://52.74.127.225/POSAPIS/api/OrderDetail?mobileNo=";
	final String	URL_RESTAURENT           	= "http://52.74.127.225/POSAPIS/api/GetOutletList";

}
