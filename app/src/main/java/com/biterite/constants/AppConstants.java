package com.biterite.constants;

public interface AppConstants {



	public String				EXTRA_LOOKS							= "extra_looks";
	public String				EXTRA_PHONE_NO						= "extra_phone_no";
	public String				EXTRA_USERNAME						="extra_username";
	public static final String	NIGHT_ADVISOR_PREFS					= "bite_rite";
	public String 				FROM_TROUBLE 						= "extra_trouble";


}
