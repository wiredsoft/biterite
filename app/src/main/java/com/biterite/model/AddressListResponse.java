package com.biterite.model;

/**
 * Created by Saurabh Vardani on 10-11-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class AddressListResponse {

    @SerializedName("userAddresslist")
    @Expose
    private List<UserAddresslist> userAddresslist = new ArrayList<UserAddresslist>();
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;

    /**
     * @return The userAddresslist
     */
    public List<UserAddresslist> getUserAddresslist() {
        return userAddresslist;
    }

    /**
     * @param userAddresslist The userAddresslist
     */
    public void setUserAddresslist(List<UserAddresslist> userAddresslist) {
        this.userAddresslist = userAddresslist;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The statusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public class UserAddresslist {

        @SerializedName("Name")
        @Expose
        private String name;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("pinCode")
        @Expose
        private String pinCode;

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The Name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The state
         */
        public String getState() {
            return state;
        }

        /**
         * @param state The state
         */
        public void setState(String state) {
            this.state = state;
        }

        /**
         * @return The city
         */
        public String getCity() {
            return city;
        }

        /**
         * @param city The city
         */
        public void setCity(String city) {
            this.city = city;
        }

        /**
         * @return The location
         */
        public String getLocation() {
            return location;
        }

        /**
         * @param location The location
         */
        public void setLocation(String location) {
            this.location = location;
        }

        /**
         * @return The address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address The address
         */
        public void setAddress(String address) {
            this.address = address;
        }

        /**
         * @return The pinCode
         */
        public String getPinCode() {
            return pinCode;
        }

        /**
         * @param pinCode The pinCode
         */
        public void setPinCode(String pinCode) {
            this.pinCode = pinCode;
        }

    }

}