package com.biterite.model;

import java.util.List;

/**
 * Created by Saurabh Vardani on 11-11-2016.
 */

public class MobileOrderListResponse {

    private List<AddToCartResponse> addToCartResponseList;

    public List<AddToCartResponse> getAddToCartResponseList() {
        return addToCartResponseList;
    }

    public void setAddToCartResponseList(List<AddToCartResponse> addToCartResponseList) {
        this.addToCartResponseList = addToCartResponseList;
    }
}
