package com.biterite.model;

/**
 * Created by saurabh vardani on 17-10-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RestaurentMenuResponse {

    @SerializedName("productlist")
    @Expose
    private List<Productlist> productlist = new ArrayList<Productlist>();
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;

    /**
     * @return The productlist
     */
    public List<Productlist> getProductlist() {
        return productlist;
    }

    /**
     * @param productlist The productlist
     */
    public void setProductlist(List<Productlist> productlist) {
        this.productlist = productlist;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The statusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public class Productlist {

        @SerializedName("productTypeCode")
        @Expose
        private String productTypeCode;
        @SerializedName("productTypeName")
        @Expose
        private String productTypeName;

        /**
         * @return The productTypeCode
         */
        public String getProductTypeCode() {
            return productTypeCode;
        }

        /**
         * @param productTypeCode The productTypeCode
         */
        public void setProductTypeCode(String productTypeCode) {
            this.productTypeCode = productTypeCode;
        }

        /**
         * @return The productTypeName
         */
        public String getProductTypeName() {
            return productTypeName;
        }

        /**
         * @param productTypeName The productTypeName
         */
        public void setProductTypeName(String productTypeName) {
            this.productTypeName = productTypeName;
        }

    }
}