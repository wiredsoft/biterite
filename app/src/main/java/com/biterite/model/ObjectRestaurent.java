package com.biterite.model;

/**
 * Created by K.Agg on 12-01-2017.
 */

public class ObjectRestaurent {

    private String outLetCode;
    private String outLetName;
    private String townName;

    public String getOutLetCode() {
        return outLetCode;
    }

    public void setOutLetCode(String outLetCode) {
        this.outLetCode = outLetCode;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    public String getOutLetName() {
        return outLetName;
    }

    public void setOutLetName(String outLetName) {
        this.outLetName = outLetName;
    }
}
