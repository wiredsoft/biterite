package com.biterite.model;

import java.util.List;

/**
 * Created by K.Agg on 12-11-2016.
 */

public class AddToCartListResponse {

    private List<AddToCartResponse> MobileOrderList;

    public List<AddToCartResponse> getAddToCartResponses() {
        return MobileOrderList;
    }

    public void setAddToCartResponses(List<AddToCartResponse> addToCartResponses) {
        this.MobileOrderList = addToCartResponses;
    }
}
