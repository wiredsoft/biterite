package com.biterite.model;

import java.util.List;

/**
 * Created by K.Agg on 12-01-2017.
 */

public class RestaurentListModel {


    private List<ObjectRestaurent> objOutList;
    private int statusCode;
    private String statusMessage;


    public List<ObjectRestaurent> getObjOutList() {
        return objOutList;
    }

    public void setObjOutList(List<ObjectRestaurent> objOutList) {
        this.objOutList = objOutList;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
