package com.biterite.model;

/**
 * Created by saurabh vardani on 18-10-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class RestaurentProductMenuResponse {

    @SerializedName("productlstbycatID")
    @Expose
    private List<ProductlstbycatID> productlstbycatID = new ArrayList<ProductlstbycatID>();
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;

    /**
     * @return The productlstbycatID
     */
    public List<ProductlstbycatID> getProductlstbycatID() {
        return productlstbycatID;
    }

    /**
     * @param productlstbycatID The productlstbycatID
     */
    public void setProductlstbycatID(List<ProductlstbycatID> productlstbycatID) {
        this.productlstbycatID = productlstbycatID;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The statusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public class ProductlstbycatID {

        @SerializedName("productType")
        @Expose
        private String productType;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("productName")
        @Expose
        private String productName;
        @SerializedName("productRate")
        @Expose
        private String productRate;

        /**
         * @return The productType
         */
        public String getProductType() {
            return productType;
        }

        /**
         * @param productType The productType
         */
        public void setProductType(String productType) {
            this.productType = productType;
        }

        /**
         * @return The productCode
         */
        public String getProductCode() {
            return productCode;
        }

        /**
         * @param productCode The productCode
         */
        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        /**
         * @return The productName
         */
        public String getProductName() {
            return productName;
        }

        /**
         * @param productName The productName
         */
        public void setProductName(String productName) {
            this.productName = productName;
        }

        /**
         * @return The productRate
         */
        public String getProductRate() {
            return productRate;
        }

        /**
         * @param productRate The productRate
         */
        public void setProductRate(String productRate) {
            this.productRate = productRate;
        }

    }

}