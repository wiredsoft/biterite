package com.biterite.model;

/**
 * Created by Saurabh Vardani on 14-11-2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PreviousOrderResponse {

    @SerializedName("objUserOrder")
    @Expose
    private List<ObjUserOrder> objUserOrder = new ArrayList<ObjUserOrder>();
    @SerializedName("statusCode")
    @Expose
    private int statusCode;
    @SerializedName("statusMessage")
    @Expose
    private String statusMessage;

    /**
     * @return The objUserOrder
     */
    public List<ObjUserOrder> getObjUserOrder() {
        return objUserOrder;
    }

    /**
     * @param objUserOrder The objUserOrder
     */
    public void setObjUserOrder(List<ObjUserOrder> objUserOrder) {
        this.objUserOrder = objUserOrder;
    }

    /**
     * @return The statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage The statusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public class ObjUserOrder {

        @SerializedName("orderNumber")
        @Expose
        private int orderNumber;
        @SerializedName("personName")
        @Expose
        private String personName;
        @SerializedName("mobileNo")
        @Expose
        private String mobileNo;
        @SerializedName("itemCode")
        @Expose
        private String itemCode;
        @SerializedName("quantity")
        @Expose
        private int quantity;
        @SerializedName("amount")
        @Expose
        private double amount;
        @SerializedName("totalAmount")
        @Expose
        private double totalAmount;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("itemName")
        @Expose
        private String itemName;
        @SerializedName("orderDate")
        @Expose
        private String orderDate;
        @SerializedName("orderStatus")
        @Expose
        private String orderStatus;
        @SerializedName("address")
        @Expose
        private String address;

        /**
         * @return The orderNumber
         */
        public int getOrderNumber() {
            return orderNumber;
        }

        /**
         * @param orderNumber The orderNumber
         */
        public void setOrderNumber(int orderNumber) {
            this.orderNumber = orderNumber;
        }

        /**
         * @return The personName
         */
        public String getPersonName() {
            return personName;
        }

        /**
         * @param personName The personName
         */
        public void setPersonName(String personName) {
            this.personName = personName;
        }

        /**
         * @return The mobileNo
         */
        public String getMobileNo() {
            return mobileNo;
        }

        /**
         * @param mobileNo The mobileNo
         */
        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        /**
         * @return The itemCode
         */
        public String getItemCode() {
            return itemCode;
        }

        /**
         * @param itemCode The itemCode
         */
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        /**
         * @return The quantity
         */
        public int getQuantity() {
            return quantity;
        }

        /**
         * @param quantity The quantity
         */
        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        /**
         * @return The amount
         */
        public double getAmount() {
            return amount;
        }

        /**
         * @param amount The amount
         */
        public void setAmount(double amount) {
            this.amount = amount;
        }

        /**
         * @return The totalAmount
         */
        public double getTotalAmount() {
            return totalAmount;
        }

        /**
         * @param totalAmount The totalAmount
         */
        public void setTotalAmount(double totalAmount) {
            this.totalAmount = totalAmount;
        }

        /**
         * @return The paymentMode
         */
        public String getPaymentMode() {
            return paymentMode;
        }

        /**
         * @param paymentMode The paymentMode
         */
        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        /**
         * @return The itemName
         */
        public String getItemName() {
            return itemName;
        }

        /**
         * @param itemName The itemName
         */
        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        /**
         * @return The orderDate
         */
        public String getOrderDate() {
            return orderDate;
        }

        /**
         * @param orderDate The orderDate
         */
        public void setOrderDate(String orderDate) {
            this.orderDate = orderDate;
        }

        /**
         * @return The orderStatus
         */
        public String getOrderStatus() {
            return orderStatus;
        }

        /**
         * @param orderStatus The orderStatus
         */
        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        /**
         * @return The address
         */
        public String getAddress() {
            return address;
        }

        /**
         * @param address The orderNumber
         */
        public void setAddress(String address) {
            this.address = address;
        }

    }

}