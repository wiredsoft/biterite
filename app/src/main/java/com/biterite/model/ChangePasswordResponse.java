package com.biterite.model;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 15/09/16.
 */
public class ChangePasswordResponse extends CommonJsonResponse {

    private int statusCode;
    private String statusMessage;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
