package com.biterite.model;

/**
 * Created by K.Agg on 18-10-2016.
 */
public class AddToCartModel {

    private String productName;
    private String productQuantity;
    private String productTypeName;
    private String restaurentId;
    private String productId;
    private float productAmount;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getRestaurentId() {
        return restaurentId;
    }

    public void setRestaurentId(String restaurentId) {
        this.restaurentId = restaurentId;
    }

    public float getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(float productAmount) {
        this.productAmount = productAmount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public String getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(String productQuantity) {
        this.productQuantity = productQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddToCartModel that = (AddToCartModel) o;
        return productName.equals(that.productName);
    }
}
