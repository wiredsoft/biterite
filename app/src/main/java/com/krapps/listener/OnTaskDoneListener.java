package com.krapps.listener;

public interface OnTaskDoneListener {
	void onTaskDone();
}
